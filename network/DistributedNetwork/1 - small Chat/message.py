from direct.distributed.DistributedObject import DistributedObject
from direct.showbase.MessengerGlobal import messenger

class Message(DistributedObject):
    def __init__(self, clientRepo):
        DistributedObject.__init__(self, clientRepo)

    ## def generate(self):
    ##     """ This method is called when the object is generated: when it
    ##     manifests for the first time on a particular client, or when it
    ##     is pulled out of the cache after a previous manifestation.  At
    ##     the time of this call, the object has been created, but its
    ##     required fields have not yet been filled in. """
    ##     # Always call up to parent class
    ##     DistributedObject.generate(self)

    def announceGenerate(self):
        """ This method is called after generate(), after all of the
        required fields have been filled in.  At the time of this call,
        the distributed object is ready for use. """
        DistributedObject.announceGenerate(self)
        # Now that the object has been fully manifested, we can do
        # all things with it.

    def disable(self):
        """ This method is called when the object is removed from the
        scene, for instance because it left the zone.  It is balanced
        against generate(): for each generate(), there will be a
        corresponding disable().  Everything that was done in
        generate() or announceGenerate() should be undone in disable().

        After a disable(), the object might be cached in memory in case
        it will eventually reappear.  The DistributedObject should be
        prepared to receive another generate() for an object that has
        already received disable().

        Note that the above is only strictly true for *cacheable*
        objects.  Most objects are, by default, non-cacheable; you
        have to call obj.setCacheable(True) (usually in the
        constructor) to make it cacheable.  Until you do this, your
        non-cacheable object will always receive a delete() whenever
        it receives a disable(), and it will never be stored in a
        cache.
        """
        # Things to call to disable the object
        DistributedObject.disable(self)

    def delete(self):
        """ This method is called after disable() when the object is to
        be completely removed, for instance because the other user
        logged off.  We will not expect to see this object again; it
        will not be cached.  This is stronger than disable(), and the
        object may remove any structures it needs to in order to allow
        it to be completely deleted from memory.  This balances against
        __init__(): every DistributedObject that is created will
        eventually get delete() called for it exactly once. """
        # Cleanup code goes here
        DistributedObject.delete(self)

    def sendText(self, messageText):
        """Function which is caled for local changes only"""
        # send an event, which will set the text on the
        #print "got a message"
        messenger.send("setText", [messageText])

    def d_sendText(self, messageText):
        """Function which is caled to send the message over the network
        therfore the d_ suffix stands for distributed"""
        #print "send message %s" % messageText
        self.sendUpdate("sendText", [messageText])

    def b_sendText(self, messageText):
        """Function which combines the local and distributed functionality,
        so the sendText and d_sendText functions are called.
        The b_ suffix stands for both"""
        self.sendText(messageText)
        self.d_sendText(messageText)
