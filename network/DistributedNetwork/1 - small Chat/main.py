#!/usr/bin/python

""" Demonstrates the DC system in form of a small chat application. """

# import the sys module to access the sys.exit function
import sys
from pandac.PandaModules import loadPrcFileData
# set the window size apropriate
loadPrcFileData("", "win-size 300 500")


################################################################################


# --------------------
# Engine dependencies
# --------------------
# all imports needed by the engine itselfe
from direct.showbase.ShowBase import ShowBase
class Main(ShowBase):
    """Main class of the application
    initialise the engine (ShowBase)"""

    def __init__(self):
        """initialise the engine"""
        ShowBase.__init__(self)
# instantiate the engine
base = Main()


################################################################################


# -------------------------
# Server side dependencies
# -------------------------
# all imports needed by the server
from direct.distributed.ServerRepository import ServerRepository
# the main server class
class MyServerRepository(ServerRepository):
    """The server repository class"""
    def __init__(self):
        """initialise the server class"""
        # get the port number from the configuration file
        # if it doesn't exist, use 4400 as the default
        tcpPort = base.config.GetInt("server-port", 4400)
        # list of all needed .dc files
        dcFileNames = ["direct.dc", "net.dc"]
        # initialise the server on this machine with the
        # port number and the dc filenames
        ServerRepository.__init__(self, tcpPort, dcFileNames = dcFileNames)


################################################################################


# -------------------------
# Client side dependencies
# -------------------------
# all imports needed by the client
from direct.showbase.DirectObject import DirectObject
from direct.distributed.ClientRepository import ClientRepository
from pandac.PandaModules import URLSpec
from message import Message
# the client repository class
class MyClientRepository(ClientRepository):
    def __init__(self):
        # list of all needed .dc files
        dcFileNames = ["direct.dc", "net.dc"]
        # initialise the client repository on this
        # machine with the dc filenames
        ClientRepository.__init__(self, dcFileNames = dcFileNames)

# the main client class
class Client(DirectObject):
    """The main client class, which contains all the logic
    and stuff you can see in the application and handles the
    connection to the server"""
    def __init__(self):
        """ Default constructor for the Clinet class """
        # msg is the class which will be send over the Network
        # to transmit our messages
        self.msg = None
        # get the port number from the configuration file
        # if it doesn't exist, use 4400 as the default
        tcpPort = base.config.GetInt("server-port", 4400)
        # get the host name from the configuration file
        # which we want to connect to. If it doesn't exit
        # we use loopback to connect to
        hostname = base.config.GetString("server-host", "127.0.0.1")
        # now build the url from the data given above
        self.url = URLSpec("http://%s:%s" % (hostname, tcpPort))

        # create the Repository for the client
        self.cr = MyClientRepository()
        # and finaly try to connect to the server
        self.cr.connect([self.url],
                        successCallback = self.connectSuccess,
                        failureCallback = self.connectFailure)

    def connectFailure(self, statusCode, statusString):
        """ some error occured while try to connect to the server """
        raise Exception("Failed to connect to %s: %s."
                        % (self.url, statusString))

    def connectSuccess(self):
        """ Successfully connected.  But we still can't really do
        anything until we've got the doID range. """
        print("Connection established, waiting for server.")
        # now wait until the server sends us the createReady message
        self.acceptOnce("createReady", self.createReady)

    def createReady(self):
        """ Now we're ready to go! """

        # create a instance of the message class
        msg = Message(self.cr)
        # and create the distributed Object with it
        self.cr.createDistributedObject(
            distObj = msg, zoneId = 1)
        # save the created Distributed Object
        # in self.msg for later usage
        self.msg = msg

        # set GUI events
        # clear out the input field for new Mesages
        # if the user focus it
        txt_msg["focusInCommand"] = self.clearText
        # set the default text again, if the Textfield
        # lost its focus
        txt_msg["focusOutCommand"] = self.setDefaultText
        # set send message commands to the textfield and
        # the button
        txt_msg["command"] = self.send
        btn_send["command"] = self.send
        # set the extraArgs to None, so the text from
        # the textfield will be used as message
        # if the button is used for sending
        btn_send["extraArgs"] = [None]

        # show all controls for displaing and sending
        # our messages
        btn_send.show()
        txt_msg.show()
        txt_messages.show()

    def clearText(self):
        """ Write an empty string in the textbox """
        txt_msg.enterText("")

    def setDefaultText(self):
        """ Write the default message in the textbox """
        txt_msg.enterText("Your Message")

    def send(self, msg):
        """Function to call the send function of the message class,
        which sends the Message over the Network to the other users"""
        text = ""
        if msg != None:
            text = msg
        else:
            text = txt_msg.get()
        self.msg.b_sendText(text)
        txt_msg.enterText('')


################################################################################


# -----------------
# creating the GUI
# -----------------
# imports for the GUI system
from direct.gui.DirectGui import DirectButton
from direct.gui.DirectGui import DirectEntry
from direct.gui.DirectGui import DirectFrame

# Functions which we will attach to the buttons
def start_server():
    """starting the server"""
    # instantiate the server
    MyServerRepository()
    # give some feedback to the user
    print("server created, waiting for client.")
    btn_server.hide()

def start_client():
    """starting the client"""
    # instantiate the client
    Client()
    print("client created")
    btn_client.hide()
    btn_server.hide()

# some size and position calculations which occure
# if the application window is resized
ratioX = 1.0
ratioY = 1.0
def recalcAspectRatio():
    props = base.win.getProperties()
    ratioX = 1.0
    ratioY = 1.0
    if props.getXSize() > props.getYSize():
        ratioX = (props.getXSize() * 1.0 /
                  props.getYSize() * 1.0)
    else:
        ratioY = (props.getYSize() * 1.0 /
                  props.getXSize() * 1.0)
    frameMain["frameSize"] = (
        -1.0*ratioX, 1.0*ratioX,
        -1.0*ratioY, 1.0*ratioY)
    txt_messages.setPos(-0.7*ratioX,0,0.5*ratioY)
    btn_send.setPos(0.75*ratioX,0,-0.9*ratioY)
    txt_msg.setPos(-0.9*ratioX,0,-0.9*ratioY)

# Function which will write the given text in the
# textbox, where we store all send and recieved messages.
# This Function will only write the last 10 messages and
# cut of the erlier messages
def setText(messageText):
    # get all messages from the textbox
    parts = txt_messages.get().split("\n")
    if len(parts) >= 10:
        cutParts = ""
        # as the textbox can only hold 10 lines cut out the first entry
        for i in range(1,len(parts)):
            cutParts += parts[i] + "\n"
        txt_messages.enterText(cutParts + messageText)
    else:
        txt_messages.enterText(txt_messages.get() + "\n" + messageText)

def exitApp():
    sys.exit()

# create a DirectObject instance, which will then catch the events and
# handle them with the given functions
do = DirectObject()
do.accept("aspectRatioChanged", recalcAspectRatio) # the window size changed
do.accept("setText", setText) # a messeg, we throw when we want to
do.accept("escape", exitApp) # exit the application if the user hit the Esc key

# this frame will contain all our GUI elements
frameMain = DirectFrame(
    # size of the frame
    frameSize = (-1*ratioX, 1*ratioX,
                 -1*ratioY, 1*ratioY),
    # position of the frame
    pos = (0, 0, 0),
    # temporarly bg color
    frameColor = (0, 0, 0, 1))

# this button will start the server
btn_server = DirectButton(text = "Start Server", pos = (0,0,0.25),
                          scale=0.15, command=start_server)
# and this one the Client
btn_client = DirectButton(text = "Start client", pos = (0,0,-0.25),
                          scale=0.15, command=start_client)

# now we set up the GUI for sending and displaing messages
# the Button which we use to send the Message to the other users
btn_send = DirectButton(
    text = "Send",
    pos = (0.75*ratioX,0,-0.9*ratioY),
    scale = 0.15)

# the Textbox where we write our Messages, which we
# want to send over to the other users
txt_msg = DirectEntry(
    initialText = "Your Message",
    cursorKeys = True,
    pos = (-0.9*ratioX,0,-0.9*ratioY),
    scale = 0.1,
    width = 14)

# the Textbox which stores all the messages we send
# or recieved over the Network
txt_messages = DirectEntry(
    cursorKeys = False,
    pos = (-0.8*ratioX,0,0.5*ratioY),
    scale = 0.1,
    width = 14,
    numLines = 10)

# reparent all elements to the main frame
btn_server.reparentTo(frameMain)
btn_client.reparentTo(frameMain)
btn_send.reparentTo(frameMain)
txt_msg.reparentTo(frameMain)
txt_messages.reparentTo(frameMain)

# recalc the ratio of the elements to place and scale
# them according to the window size
recalcAspectRatio()

# hide unnecessary elements at startup
btn_send.hide()
txt_msg.hide()
txt_messages.hide()


################################################################################


# --------------------
# Application starter
# --------------------
# starting the application
if __name__ == "__main__":
    # start the application
    base.run()
