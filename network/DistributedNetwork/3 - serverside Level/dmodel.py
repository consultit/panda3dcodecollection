from direct.distributed.DistributedObject import DistributedObject
import random

class DModel(DistributedObject):
    def __init__(self, cr):
        DistributedObject.__init__(self, cr)
        self.model = base.loader.loadModel("smiley.egg")

    def setInitialPos(self, x, y, z):
        self.initialPos = (x, y, z)

    def getInitialPos(self):
        return self.initialPos

    def announceGenerate(self):
        DistributedObject.announceGenerate(self)
        x = 0
        y = random.randint(0, 5)* 10
        z = random.randint(0, 5) * 10
        print("x=%02d y=%02d z=%02d" % (x, y, z))
        self.model.setPos(x,y,z)
        self.model.reparentTo(base.render)

    def disable(self):
        self.model.detachNode()
        DistributedObject.disable(self)
