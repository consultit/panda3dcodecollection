from direct.distributed.DistributedObjectAI import DistributedObjectAI

class DLevelAI(DistributedObjectAI):

    """ This is the AI-side implementation of DLevel. """

    def __init__(self, cr):
        DistributedObjectAI.__init__(self, cr)
