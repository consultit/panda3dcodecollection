#!/usr/bin/python

""" Demonstrates the DC system in form of a small chat application. """

# import the sys module to access the sys.exit function
import sys

# various imports
# those imports are needed in various parts of this application


################################################################################


# --------------------
# Engine dependencies
# --------------------
# all imports needed by the engine itselfe
from direct.showbase.ShowBase import ShowBase
class Main(ShowBase):
    """Main class of the application
    initialise the engine (ShowBase)"""

    def __init__(self):
        """initialise the engine"""
        ShowBase.__init__(self)
# instantiate the engine
base = Main()


################################################################################


# -------------------------
# Server side dependencies
# -------------------------
# all imports needed by the server
from direct.distributed.ServerRepository import ServerRepository
# the following import is needed because we need
# a client on the Server for the AI Classes
from direct.distributed.ClientRepository import ClientRepository
# the main server class
class MyServerRepository(ServerRepository):
    """The server repository class"""
    def __init__(self):
        """initialise the server class"""
        # get the port number from the configuration file
        # if it doesn't exist, use 4400 as the default
        tcpPort = base.config.GetInt("server-port", 4400)
        # list of all needed .dc files
        dcFileNames = ["direct.dc", "net.dc"]
        # initialise the server on this machine with the
        # port number and the dc filenames
        ServerRepository.__init__(self, tcpPort, dcFileNames = dcFileNames)


# a small client only for the Server, which holds the Level model
class LevelAIRepository(ClientRepository):
    def __init__(self):
        dcFileNames = ["direct.dc", "net.dc"]

        ClientRepository.__init__(self, dcFileNames = dcFileNames,
                                  dcSuffix = "AI")

        tcpPort = base.config.GetInt("server-port", 4400)
        url = URLSpec("http://127.0.0.1:%s" % (tcpPort))
        self.connect([url],
                     successCallback = self.connectSuccess,
                     failureCallback = self.connectFailure)

    def connectFailure(self, statusCode, statusString):
        raise StandardError(statusString)

    def connectSuccess(self):
        """ Successfully connected.  But we still can't really do
        anything until we've got the doID range. """
        print("start now")
        self.acceptOnce("createReady", self.createReady)

    def createReady(self):
        """ Now we're ready to go! """

        print("create the level")
        self.level = self.createDistributedObject(
            className = "DLevelAI", zoneId = 1)
        print("level finished")



################################################################################


# -------------------------
# Client side dependencies
# -------------------------
# all imports needed by the client
from direct.showbase.DirectObject import DirectObject
from pandac.PandaModules import URLSpec
# the client repository class
class MyClientRepository(ClientRepository):
    def __init__(self):
        # list of all needed .dc files
        dcFileNames = ["direct.dc", "net.dc"]
        # initialise the client repository on this
        # machine with the dc filenames
        ClientRepository.__init__(self, dcFileNames = dcFileNames)


# the main client class
class Client(DirectObject):
    """The main client class, which contains all the logic
    and stuff you can see in the application and handles the
    connection to the server"""
    def __init__(self):
        """ Default constructor for the Clinet class """
        # this is our model which will be seen on the screen
        print("client start")
        self.model = None
        # get the port number from the configuration file
        # if it doesn't exist, use 4400 as the default
        tcpPort = base.config.GetInt("server-port", 4400)
        # get the host name from the configuration file
        # which we want to connect to. If it doesn't exit
        # we use loopback to connect to
        hostname = base.config.GetString("server-host", "127.0.0.1")
        # now build the url from the data given above
        self.url = URLSpec("http://%s:%s" % (hostname, tcpPort))

        # create the Repository for the client
        self.cr = MyClientRepository()
        # and finaly try to connect to the server
        self.cr.connect([self.url],
                        successCallback = self.connectSuccess,
                        failureCallback = self.connectFailure)

    def connectFailure(self, statusCode, statusString):
        """ some error occured while try to connect to the server """
        raise Exception("Failed to connect to %s: %s."
                        % (self.url, statusString))

    def connectSuccess(self):
        """ Successfully connected.  But we still can't really do
        anything until we've got the doID range. """
        print("Connection established, waiting for server.")
        # now wait until the server sends us the createReady message
        self.acceptOnce("createReady", self.createReady)

    def createReady(self):
        """ Now we're ready to go! """
        # Nothing to do here...
        self.model = self.cr.createDistributedObject(
            className = "DModel", zoneId = 1)


################################################################################


# -----------------
# creating the GUI
# -----------------
# imports for the GUI system
from direct.gui.DirectGui import DirectButton

# Functions which we will attach to the buttons
def start_server():
    """starting the server"""
    # instantiate the server
    MyServerRepository()
    # instantiate the Level
    LevelAIRepository()
    # give some feedback to the user
    print("server created, waiting for client.")
    btn_server.hide()

def start_client():
    """starting the client"""
    # instantiate the client
    Client()
    print("client created")
    btn_client.hide()
    btn_server.hide()

def exitApp():
    sys.exit()

# create a DirectObject instance, which will then catch the events and
# handle them with the given functions
do = DirectObject()
do.accept("escape", exitApp) # exit the application if the user hit the Esc key

# this button will start the server
btn_server = DirectButton(text = "Start Server", pos = (0,0,0.25),
                          scale=0.15, command=start_server)
# and this one the Client
btn_client = DirectButton(text = "Start client", pos = (0,0,-0.25),
                          scale=0.15, command=start_client)


################################################################################


# --------------------
# Application starter
# --------------------
# starting the application
if __name__ == "__main__":
    # start the application
    base.run()
