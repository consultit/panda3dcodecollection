#!/usr/bin/python

""" Demonstrates the DC system in form of a small chat application. """

# import the sys module to access the sys.exit function
import sys

# various imports
# those imports are needed in various parts of this application


################################################################################


# --------------------
# Engine dependencies
# --------------------
# all imports needed by the engine itselfe
from direct.showbase.ShowBase import ShowBase
class Main(ShowBase):
    """Main class of the application
    initialise the engine (ShowBase)"""

    def __init__(self):
        """initialise the engine"""
        ShowBase.__init__(self)
# instantiate the engine
base = Main()


################################################################################


# -------------------------
# Server side dependencies
# -------------------------
# all imports needed by the server
from direct.distributed.ServerRepository import ServerRepository
# the main server class
class MyServerRepository(ServerRepository):
    """The server repository class"""
    def __init__(self):
        """initialise the server class"""
        # get the port number from the configuration file
        # if it doesn't exist, use 4400 as the default
        tcpPort = base.config.GetInt("server-port", 4400)
        # list of all needed .dc files
        dcFileNames = ["direct.dc", "net.dc"]
        # initialise the server on this machine with the
        # port number and the dc filenames
        ServerRepository.__init__(self, tcpPort, dcFileNames = dcFileNames)


################################################################################


# -------------------------
# Client side dependencies
# -------------------------
# all imports needed by the client
from direct.showbase.DirectObject import DirectObject
from direct.distributed.ClientRepository import ClientRepository
from pandac.PandaModules import URLSpec
# the client repository class
class MyClientRepository(ClientRepository):
    def __init__(self):
        # list of all needed .dc files
        dcFileNames = ["direct.dc", "net.dc"]
        # initialise the client repository on this
        # machine with the dc filenames
        ClientRepository.__init__(self, dcFileNames = dcFileNames)

class MyAIRepository(ClientRepository):
    def __init__(self):
        dcFileNames = ["direct.dc", "net.dc"]

        ClientRepository.__init__(self, dcFileNames = dcFileNames,
                                  dcSuffix = "AI")

        tcpPort = base.config.GetInt("server-port", 4400)
        url = URLSpec("http://127.0.0.1:%s" % (tcpPort))
        self.connect([url],
                     successCallback = self.connectSuccess,
                     failureCallback = self.connectFailure)

    def connectFailure(self, statusCode, statusString):
        raise StandardError(statusString)

    def connectSuccess(self):
        """ Successfully connected.  But we still can't really do
        anything until we've got the doID range. """
        self.acceptOnce("createReady", self.createReady)

    def createReady(self):
        """ Now we're ready to go! """
        # Put the time manager in zone 1 where the clients can find it.
        self.timeManager = self.createDistributedObject(
            className = "TimeManagerAI", zoneId = 1)


# the main client class
class Client(DirectObject):
    """The main client class, which contains all the logic
    and stuff you can see in the application and handles the
    connection to the server"""

    # Degrees per second of rotation
    rotateSpeed = 90

    # Units per second of motion
    moveSpeed = 8

    def __init__(self):
        """ Default constructor for the Clinet class """
        # The list of keys that we will be monitoring.
        self.moveKeyList = [
            "arrow_left", "arrow_right", "arrow_up", "arrow_down"
            ]
        # Initially, all keys are up.  Construct a dictionary that
        # maps each of the above keys to False, and hang the event to
        # manage that state.
        self.moveKeys = {}
        for key in self.moveKeyList:
            self.moveKeys[key] = False
            self.accept(key,
                        self.moveKeyStateChanged,
                        extraArgs = [key, True])
            self.accept(key + "-up",
                        self.moveKeyStateChanged,
                        extraArgs = [key, False])
        # this is our model which will be seen on the screen
        self.model = None
        # get the port number from the configuration file
        # if it doesn't exist, use 4400 as the default
        tcpPort = base.config.GetInt("server-port", 4400)
        # get the host name from the configuration file
        # which we want to connect to. If it doesn't exit
        # we use loopback to connect to
        hostname = base.config.GetString("server-host", "127.0.0.1")
        # now build the url from the data given above
        self.url = URLSpec("http://%s:%s" % (hostname, tcpPort))

        # create the Repository for the client
        self.cr = MyClientRepository()
        # and finaly try to connect to the server
        self.cr.connect([self.url],
                        successCallback = self.connectSuccess,
                        failureCallback = self.connectFailure)

    def moveKeyStateChanged(self, key, newState):
        """ A key event has been received.  Change the key state in
        the dictionary. """
        self.moveKeys[key] = newState

    def connectFailure(self, statusCode, statusString):
        """ some error occured while try to connect to the server """
        raise Exception("Failed to connect to %s: %s."
                        % (self.url, statusString))

    def connectSuccess(self):
        """ Successfully connected.  But we still can't really do
        anything until we've got the doID range. """
        print("Connection established, waiting for server.")
        self.cr.setInterestZones([1])
        # now wait until the server sends us the createReady message
        self.acceptOnce("gotTimeSync", self.syncReady)

    def syncReady(self):
        """ Now we've got the TimeManager manifested, and we're in
        sync with the server time. Now we can enter the world. Check
        to see if we've received our doIdBase yet. """
        if self.cr.haveCreateAuthority():
            self.createReady()
        else:
            # Not yet, keep waiting a bit longer.
            self.acceptOnce("createReady", self.createReady)

    def createReady(self):
        """ Now we're ready to go! """
        # Create the distributed Model Object
        self.model = self.cr.createDistributedObject(
            className = "DModel", zoneId = 101)

        # Update the local avatar's position every frame.
        self.moveTask = taskMgr.add(self.moveAvatar, "moveAvatar")

        # Let the DistributedSmoothNode take care of broadcasting the
        # position updates several times a second.
        self.model.startPosHprBroadcast()

    def moveAvatar(self, task):
        """ This task runs each frame to move the avatar according to
        the set of arrow keys that are being held. """

        dt = globalClock.getDt()

        if self.moveKeys["arrow_left"]:
            self.model.setH(self.model, dt * self.rotateSpeed)
        elif self.moveKeys["arrow_right"]:
            self.model.setH(self.model, -dt * self.rotateSpeed)

        if self.moveKeys["arrow_up"]:
            self.model.setY(self.model, dt * self.moveSpeed)
        elif self.moveKeys["arrow_down"]:
            self.model.setY(self.model, -dt * self.moveSpeed)

        return task.cont

################################################################################


# -----------------
# creating the GUI
# -----------------
# imports for the GUI system
from direct.gui.DirectGui import DirectButton

# Functions which we will attach to the buttons
def start_server():
    """starting the server"""
    # instantiate the server
    MyServerRepository()
    # instantiate the AI Repository for the Time sync
    MyAIRepository()
    # give some feedback to the user
    print("server created, waiting for client.")
    btn_server.hide()

def start_client():
    """starting the client"""
    # instantiate the client
    Client()
    print("client created")
    btn_client.hide()
    btn_server.hide()

def exitApp():
    sys.exit()

# create a DirectObject instance, which will then catch the events and
# handle them with the given functions
do = DirectObject()
do.accept("escape", exitApp) # exit the application if the user hit the Esc key

# this button will start the server
btn_server = DirectButton(text = "Start Server", pos = (0,0,0.25),
                          scale=0.15, command=start_server)
# and this one the Client
btn_client = DirectButton(text = "Start client", pos = (0,0,-0.25),
                          scale=0.15, command=start_client)


################################################################################


# --------------------
# Application starter
# --------------------
# starting the application
if __name__ == "__main__":
    # start the application
    base.run()
