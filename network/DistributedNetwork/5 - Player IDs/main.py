#!/usr/bin/python

""" Demonstrates the Distributed system. This will be about the Client IDs """

import sys

################################################################################


# --------------------
# Engine dependencies
# --------------------
# all imports needed by the engine itselfe
from direct.showbase.ShowBase import ShowBase
class Main(ShowBase):
    """Main class of the application
    initialise the engine (ShowBase)"""

    def __init__(self):
        """initialise the engine"""
        ShowBase.__init__(self)
# instantiate the engine
base = Main()


################################################################################


# -------------------------
# Server side dependencies
# -------------------------
# all imports needed by the server
from direct.distributed.ServerRepository import ServerRepository
# the main server class
class MyServerRepository(ServerRepository):
    """The server repository class"""
    def __init__(self):
        """initialise the server class"""
        # get the port number from the configuration file
        # if it doesn't exist, use 4400 as the default
        tcpPort = base.config.GetInt("server-port", 4400)
        # list of all needed .dc files
        dcFileNames = ["direct.dc", "net.dc"]
        # initialise the server on this machine with the
        # port number and the dc filenames
        ServerRepository.__init__(self, tcpPort, dcFileNames = dcFileNames)

    def printIDs(self):
        print("clients by DoId Base")
        print(self.clientsByDoIdBase)
        clientList = self.clientsByDoIdBase
        txt = ""
        for clientID in clientList:
            client = clientList[clientID]
            txt += "Socket: " + str(client.connection.getSocket().GetSocket()) + "\n" + \
                  "IP: " + str(client.netAddress) + "\n" + \
                  "PlayerID: " + str(client.doIdBase) + "\n" + \
                  "expl. Interest zones: " + str(client.explicitInterestZoneIds) + "\n" + \
                  "curr. Interest zones: " + str(client.currentInterestZoneIds) + "\n\n"
        lbl_ClientIDs["text"] = txt


################################################################################


# -------------------------
# Client side dependencies
# -------------------------
# all imports needed by the client
from direct.showbase.DirectObject import DirectObject
from direct.distributed.ClientRepository import ClientRepository
from pandac.PandaModules import URLSpec
#from message import Message
# the client repository class
class MyClientRepository(ClientRepository):
    def __init__(self):
        # list of all needed .dc files
        dcFileNames = ["direct.dc", "net.dc"]
        # initialise the client repository on this
        # machine with the dc filenames
        ClientRepository.__init__(self, dcFileNames = dcFileNames)

# the main client class
class Client(DirectObject):
    """The main client class, which contains all the logic
    and stuff you can see in the application and handles the
    connection to the server"""
    def __init__(self):
        """Default constructor for the Clinet class"""
        # msg is the class which will be send over the Network
        # to transmit our messages
        self.msg = None
        # get the port number from the configuration file
        # if it doesn't exist, use 4400 as the default
        tcpPort = base.config.GetInt("server-port", 4400)
        # get the host name from the configuration file
        # which we want to connect to. If it doesn't exit
        # we use loopback to connect to
        hostname = base.config.GetString("server-host", "127.0.0.1")
        # now build the url from the data given above
        self.url = URLSpec("http://%s:%s" % (hostname, tcpPort))

        # create the Repository for the client
        self.cr = MyClientRepository()
        # and finaly try to connect to the server
        self.cr.connect([self.url],
                        successCallback = self.connectSuccess,
                        failureCallback = self.connectFailure)

    def connectFailure(self, statusCode, statusString):
        """some error occured while try to connect to the server"""
        raise Exception("Failed to connect to %s: %s."
                        % (self.url, statusString))

    def connectSuccess(self):
        """Successfully connected.  But we still can't really do
        anything until we've got the doID range."""
        print("Connection established, waiting for server.")
        # now wait until the server sends us the createReady message
        self.acceptOnce("createReady", self.createReady)

    def createReady(self):
        """Now the client is ready and fully usable"""
        print("Client Ready")
        # as we now have our ID we can show it on screen
        lbl_PlayerID.show()
        lbl_PlayerID["text"] = "PlayerID: " + str(self.getPlayerID())

    def getPlayerID(self):
        """Returns the ID of this clinet"""
        return self.cr.doIdBase


################################################################################


# -----------------
# creating the GUI
# -----------------
# imports for the GUI system
from direct.gui.DirectGui import DirectButton
from direct.gui.DirectGui import DirectLabel

# Functions which we will attach to the buttons
server = None
def start_server():
    """starting the server"""
    # instantiate the server
    global server
    server = MyServerRepository()
    # give some feedback to the user
    print("server created, waiting for client.")
    btn_server.hide()
    btn_showClients.show()
    lbl_ClientIDs.show()

def start_client():
    """starting the client"""
    # instantiate the client
    cl = Client()
    print("client created")
    btn_client.hide()
    btn_server.hide()
    print(cl.getPlayerID())

def showClients():
    """This is a wraper function for
    printIDs() from the server so
    we can map it to a button"""
    server.printIDs()

def exitApp():
    sys.exit()

# create a DirectObject instance, which will then catch the events and
# handle them with the given functions
do = DirectObject()
do.accept("escape", exitApp) # exit the application if the user hit the Esc key

# this button will start the server
btn_server = DirectButton(text = "Start Server", pos = (0,0,0.25),
                          scale=0.15, command=start_server)
# and this one the Client
btn_client = DirectButton(text = "Start client", pos = (0,0,-0.25),
                          scale=0.15, command=start_client)

btn_showClients = DirectButton(text = "show all connected client", pos = (0,0,0.85),
                                scale=0.15, command=showClients)
btn_showClients.hide()

# this label shows our ID
lbl_PlayerID = DirectLabel(text = "", pos = (0,0,0.7), scale = 0.15)
lbl_PlayerID.hide()

# this label shows all clients which are connected to the server
# it will only show up at the server window
lbl_ClientIDs = DirectLabel(text = "", pos = (0,0,0.6), scale = 0.05)
lbl_ClientIDs.hide()
################################################################################


# --------------------
# Application starter
# --------------------
# starting the application
if __name__ == "__main__":
    # start the application
    base.run()

