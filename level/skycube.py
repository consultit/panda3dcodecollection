
from direct.showbase.ShowBase import ShowBase
from panda3d.core import (
    CullBinAttrib)

class SkyCube(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)

        self.accept("escape", exit)

        self.environ = self.loader.loadModel("environment")
        self.environ.reparentTo(self.render)
        self.environ.setPos(10,250,-60)


        base.camLens.setNear(0.05)
        base.cam.setP(10)

        self.skyCube = self.loader.loadModel("../data/models/skycube")
        self.skyCube.reparentTo(camera)
        self.skyCube.setDepthTest(False)
        self.skyCube.setAttrib(CullBinAttrib.make("skyCubeBin", 1000))
        taskMgr.add(self.skyCubeTask, "skycube")

    def skyCubeTask(self, task):
        self.skyCube.setHpr(render, 0,0,0)
        return task.cont

sky = SkyCube()
sky.run()
