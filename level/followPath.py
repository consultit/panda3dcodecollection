#!/usr/bin/python
# -*- coding: utf-8 -*-

# This sample shows how you can attach an object to a path and let it
# follow the path along always facing forward. It also shows how to do
# it in a loop and how to, even though not supported by YABEE, create
# cyrclic paths, by simply placing the start and endpoint perfectly ever
# each other in blender.
#
# Use F1 and F2 to switch between camera modes

from direct.directutil import Mopath
from direct.interval.MopathInterval import MopathInterval 

from direct.showbase.ShowBase import ShowBase
from panda3d.core import (
    CullBinAttrib)

class mopathSample(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)

        self.accept("escape", exit)
        self.disableMouse()
        
        self.observeCam()
        
        self.accept("f1", self.flyWithCam)
        self.accept("f2", self.observeCam)
        
        self.level = loader.loadModel("environment")
        self.level.reparentTo(render)
        self.level.setPos(0, -10, -5)
        
        # some object following a path
        self.obj_np = loader.loadModel("camera")
        self.obj_np.reparentTo(render)
        mopath_model = loader.loadModel("../data/models/moPath")
        mopath_model.reparentTo(render)
        path = mopath_model.find("**/NurbsCurve")
        path.setY(500)
        mopath = Mopath.Mopath()
        mopath.loadNodePath(path)
        mopath.fFaceForward = True
        # for debugging, draw the line of the motion path
        mopath.draw().reparentTo(render)
        
        #
        # Now we have two ways to let a Node[Path] follow the motion
        # path. Both will move the camera model we created earlier along
        # the way as well as do this in a indefinite loop
        #
        
        # 1) Let the camera model follow the path using the mopath
        #    directly
        mopath.timeScale = 5.0
        mopath.play(self.obj_np, loop=True)
        
        # 2) Let the camera model follow the path using a dedicated
        #    motion path interval
        #self.mopath_ival = MopathInterval(mopath, self.obj_np, duration=5.0, name="MoPathInterval")
        #self.mopath_ival.loop()
        
    def flyWithCam(self):
        self.camera.reparentTo(self.obj_np)
        self.camera.setPosHpr(0,0,0,0,0,0)
    
    def observeCam(self):
        self.camera.reparentTo(render)
        self.camera.setPos(5, -20, 15)
        self.camera.lookAt(0, 0, 1.5)
        
        

sample = mopathSample()
sample.run()
