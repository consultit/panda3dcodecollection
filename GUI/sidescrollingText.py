"""This app will demonstrate how to create a horizontally scrolled text."""

from direct.showbase.ShowBase import ShowBase
base = ShowBase()
from direct.gui.DirectGui import DirectScrolledFrame, DirectLabel, DGG
from panda3d.core import TextNode

# Enhance font readability
DGG.getDefaultFont().setPixelsPerUnit(75)

# create a scrolled frame that will hold the text
frame = DirectScrolledFrame(
    frameSize = (-0.75, 0.75, 0.125, -0.01),
    canvasSize = (-0.75, 0.75, 0.125, -0.01),
    # set the frames color to white
    frameColor = (0, 0, 0, 1),
    manageScrollBars = 0)
# remove the scroll bars as we won't need them here
frame.guiItem.clear_horizontal_slider()
frame.guiItem.clear_vertical_slider()
frame.horizontalScroll.removeNode()
frame.verticalScroll.removeNode()

# create a text that will be scrolled within the frame
textScale = 0.2
text = DirectLabel(
    frameColor = (0,0,0,0),
    scale = textScale,
    pos = (0.78,0,0),
    text = "=== test 1 2 3 ===",
    text_fg = (1,1,0,1),
    text_align = TextNode.ALeft)
text.setText()
# parent the text to the canvas so it get's hidden if it's outside the
# actual frame borders
text.reparentTo(frame.canvas)

# Now calculate the text size
textbounds = text.getBounds()
textwidth = (textbounds[0] + textbounds[1]) * textScale
# and create an interval that will actually move the text
# from right to left through the frame
ival = text.posInterval(5.0, (-0.75-textwidth, 0, 0))
# do this animation indefinitely
ival.loop()

# finally start the application
base.run()
