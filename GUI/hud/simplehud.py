"""This example will show a very simple player hud that contains the
players live stamina/manna and the number of collected things"""

from direct.showbase.ShowBase import ShowBase
from direct.gui.DirectGui import DirectWaitBar, DirectFrame
from direct.gui.OnscreenImage import OnscreenImage
from direct.interval.IntervalGlobal import LerpHprInterval
import os
import time

baseDir = os.path.join("..", "..", "data", "textures", "hud")

class PlayerHUD(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        #
        # Player status section
        #
        self.frameCharStatus = DirectFrame(
            # size of the frame
            frameSize = (0, .7,
                         -.20, 0),
            # bg color Transparent
            frameColor = (0, 0, 0, .25))
        self.frameCharStatus.reparentTo(base.a2dTopLeft)
        self.statusStamina = DirectWaitBar(
            text = "",
            value = 100,
            frameSize = (0.165, .65, -0.04, 0),
            pos = (0, 1, -0.05),
            barColor = (0, 0, 1, 1))
        self.statusStamina.reparentTo(self.frameCharStatus)
        self.statusHealth = OnscreenImage(
            image = os.path.join(baseDir, "playerhealthFull.png"),
            scale = (0.105, 1, 0.105),
            pos = (0.105, 0, -0.105))
        self.statusHealth.setTransparency(True)
        self.statusHealth.reparentTo(self.frameCharStatus)

    def setStaminaStatus(self, value):
        self.statusStamina["value"] = value

    def setHealthStatus(self, value):
        """this function will set the health image in the top righthand corner
        according to the given value, where value is a integer between 0 and 100
        """
        full = os.path.join(baseDir, "playerhealthFull.png")
        half = os.path.join(baseDir, "playerhealthHalf.png")
        empty = os.path.join(baseDir, "playerhealthEmpty.png")
        if value > 66:
            self.statusHealth.setImage(full)
        elif value > 33:
            self.statusHealth.setImage(half)
        else:
            self.statusHealth.setImage(empty)
        self.statusHealth.setTransparency(True)

hud = PlayerHUD()

# create an actualization task to see the hud in action
# NOTE: You should actualize the hud either with events or directly from
#       the point where it needs to be actualized and not a task that
#		will run sollely for the hud.
stamina = 100
health = 100
elapsed = 0.0
def hudtask(task):
    global elapsed
    dt = globalClock.getDt()
    elapsed += dt

    if elapsed > 0.05:
        global health
        hud.setHealthStatus(health)
        health -= 1
        if health == -1:
            health = 100

        global stamina
        hud.setStaminaStatus(stamina)
        stamina -= 1
        if stamina == -1:
            stamina = 100
        elapsed = 0.0

    return task.cont

taskMgr.add(hudtask, "test_hud")

hud.run()


