#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# PYTHON IMPORTS
#
import logging

#
# PANDA3D ENGINE IMPORTS
#
from direct.showbase.ShowBase import ShowBase
from direct.gui.DirectGui import DGG
from direct.gui.DirectGui import DirectFrame
from direct.gui.DirectGui import DirectLabel
from panda3d.core import TextNode, loadPrcFileData
from direct.gui.OnscreenImage import OnscreenImage

loadPrcFileData(
    "",
    "text-kerning true")

__author__ = "Fireclaw the Fox"
__license__ = """
Simplified BSD (BSD 2-Clause) License.
See License.txt or http://opensource.org/licenses/BSD-2-Clause for more info
"""



class MessageWriter(ShowBase):
    def __init__(self):
        # initialize the engine
        ShowBase.__init__(self)
        # set the used font
        font = loader.loadFont("./DejaVuSans.ttf")
        # Enhance font readability
        DGG.setDefaultFont(font)
        DGG.getDefaultFont().setPixelsPerUnit(75)
        # this variable should be loaded from your application settings and be
        # user defineable.
        # NOTE: The lower the value of this variable is, the faster the text is
        #       written in the textfield
        ShowBase.textWriteSpeed = 25


        # the tasktime the last sign in the textfield was written
        self.lastSign = 0.0
        # this counter holds the amount of signs that should be written
        # in the next task call
        self.globalSignCount = 0.0
        # the sign that is actually written in the textfield
        self.currentSign = 0
        # the text to write in the textfield
        self.textfieldText = ""
        # stop will be used to check if the writing is finished or
        # somehow else stoped
        self.stop = False
        # if pause is set to True, the text stops writing until pause is
        # set to False again
        self.pause = False
        # the time, how long the text is shown after it is fully written
        # only important if autocont is set
        self.showlength = 1.25
        # this function is used to check if the continue key is pressed
        self.isDown = base.mouseWatcherNode.isButtonDown
        # the following number gives the lines that will be removed from
        # top of the text when the continue key got pressed
        self.skipp_lines = 5
        # If autocont is set the continue and skip functions are disabled
        self.autocont = False
        # this variable sets the multiplier for the text speed if the
        # 'E' key is pressed
        self.writingSpeedBoost = 4.0
        # this is an indicator to not automatically continue when the 'E'
        # key was used to speed up text and the text reached the end
        self.speedKeyWasDown = False

        textStartX = 0.15
        self.textfieldWidth = abs(base.a2dLeft + textStartX) + (base.a2dRight - 0.1)

        # the textfield to put instructions hints and everything else, that
        # should be slowly written on screen and disappear after a short while
        self.textfield = TextNode('textfield')
        self.textfield.clearText()
        self.textfield.setFont(font)
        self.textfield.setShadow(0.005, 0.005)
        self.textfield.setShadowColor(0, 0, 0, 1)
        self.textfield.setWordwrap(self.textfieldWidth-0.1)
        self.textfield.setCardActual(
            -(textStartX - 0.1), self.textfieldWidth,
            0.1, base.a2dBottom+0.45)
        self.textfield.setCardColor(0, 0, 0, 0.45)
        self.textfield.setFlattenFlags(TextNode.FF_none)
        self.textfield.setTextScale(0.06)
        self.textfieldNodePath = aspect2d.attachNewNode(self.textfield)
        self.textfieldNodePath.setScale(1)
        self.textfieldNodePath.setPos(base.a2dLeft + textStartX, 0, 0.6)
        self.textfieldNodePath.reparentTo(base.a2dBottomCenter)

        self.textSkip = "Skip - Esc | Speed up - E"
        self.textSkipContinue = "Skip - Esc | Continue - E"
        self.info = DirectLabel(
            scale = 0.05,
            pos = (self.textfieldWidth - 0.025, 0, -0.525),
            frameColor = (0.3, 0.3, 0.4, 0.0),
            pad = (0.2, 0.2),
            text = self.textSkip,
            text_align = TextNode.ARight,
            text_fg = (1,1,1,1))
        self.info.reparentTo(self.textfieldNodePath)
        self.info.setTransparency(True)

        self.hide()

    def show(self):
        self.textfieldNodePath.show()
        if self.autocont:
            self.info.hide()
        else:
            self.info.show()

    def hide(self):
        self.textfield.clearText()
        self.textfieldNodePath.hide()

    def clear(self):
        """Clear the textfield and stop the current written text"""
        self.hide()
        self.reset()
        base.messenger.send("message-done")

    def cleanup(self):
        """Function that should be called to remove and reset the
        message writer"""
        self.clear()
        self.ignore("showMessage")

    def reset(self):
        taskMgr.remove("writeText")
        self.stop = False
        self.writeDone = False
        self.currentSign = 0
        self.lastSign = 0.0
        self.textfieldText = ""
        self.textfield.clearText()
        self.globalSignCount = 0.0

    def __run(self):
        """This function can be called to start the writer task."""
        self.textfield.setFlattenFlags(TextNode.FF_none)
        taskMgr.add(self.__writeText, "writeText", priority=30)
        self.accept("escape", self.clear)

    def setMessageAndShow(self, message):
        """Function to simply add a new message and show it if no other
        message is currently shown"""
        self.reset()
        logging.debug("show message %s" % message)
        self.textfieldText = message
        self.show()
        # start the writer task
        self.__run()

    def __writeText(self, task):
        elapsed = globalClock.getDt()
        if self.pause and (self.isDown("e") and not self.speedKeyWasDown):
            # continue the text
            lines = self.textfield.getWordwrappedText().split("\n")
            txt = ""
            for line in lines[self.skipp_lines:-1]:
                txt += line + "\n"
            txt += lines[-1]
            self.textfield.setText(txt)
            self.info["text"] = self.textSkip
            self.info.setText()
            self.pause = False
        elif self.pause:
            if not self.isDown("e"):
                self.speedKeyWasDown = False
            # pause the text writing
            return task.cont

        speedBoost = 1.0
        if self.stop and ((self.isDown("e") and not self.speedKeyWasDown) or self.autocont):
            # text is finished and can be cleared now
            self.info["text"] = self.textSkip
            self.info.setText()
            self.pause = False
            self.clear()
            return task.done
        elif self.isDown("e"):
            speedBoost = self.writingSpeedBoost
            self.speedKeyWasDown = True
        else:
            self.speedKeyWasDown = False


        if self.currentSign == len(self.textfieldText)-1:
            self.textfield.setFlattenFlags(TextNode.FF_dynamic_merge)

        if self.currentSign >= len(self.textfieldText):
            # check if the text is fully written
            if not self.autocont:
                # ask the user for continue
                self.info["text"] = self.textSkipContinue
                self.info.setText()
                self.stop = True
                self.textfieldNodePath.flattenStrong()
            elif task.time - self.lastSign >= self.showlength:
                # now also check if the time the text should
                # be visible on screen has elapsed
                self.stop = True
                self.textfieldNodePath.flattenStrong()
        elif not self.stop:
            # calculate how many signs should be printed
            currentSignCount = base.textWriteSpeed * speedBoost * elapsed
            self.globalSignCount += currentSignCount
            if self.globalSignCount >= 1.0:
                # calculate how many signs should be written
                numSignsToPrint = int(self.globalSignCount - self.globalSignCount % 1)
                self.globalSignCount -= numSignsToPrint
                for i in range(numSignsToPrint):
                    self.textfield.appendText(self.textfieldText[self.currentSign])#.encode("utf-8"))
                    if self.textfield.getHeight() > 0.55:
                        if not self.autocont:
                            txt = self.textfield.getText()
                            self.textfield.setText(txt[:-1])
                            self.info["text"] = self.textSkipContinue
                            self.info.setText()
                            self.pause = True
                        else:
                            lines = self.textfield.getWordwrappedText().split("\n")
                            txt = ""
                            for line in lines[1:-1]:
                                txt += line + "\n"
                            txt += lines[-1]
                            self.textfield.setText(txt)
                            self.currentSign += 1
                            self.lastSign = task.time
                    else:
                        self.currentSign += 1
                        self.lastSign = task.time
                        break
        return task.cont

writer = MessageWriter()

# Write the first text
writer.setMessageAndShow("Test Message\nWith multiple lines and special characters:\n#©ü@\nSome\nmore\ntext\nto\nshow\nscrolling\nof\nthe\ntext\nwriter.\nThis is a long line breaking text with many spaces to show how the text writer will handle new lines with spaces at the end and in front.\nEnd!")

def showGoodBye():
    # set the auto continue to True to show the differences between the
    # two modes
    writer.autocont = True
    # writ the next text
    writer.setMessageAndShow("Good bye!")
# this line shows how you can use the message-done event to for example
# line up another text to write
writer.acceptOnce("message-done", showGoodBye)

# Start the sample
writer.run()
