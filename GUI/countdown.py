from direct.directbase import DirectStart
from direct.gui.OnscreenText import OnscreenText
from direct.interval.IntervalGlobal import Sequence, Func
from panda3d.core import TextNode

number = OnscreenText(
    text = "",
    scale = 0.75,
    pos = (0, -0.375),
    fg = (0,0,0,1),
    align = TextNode.ACenter)

inter = number.scaleInterval(1.0, 0)
def setNumber(num):
    number["text"] = str(num)
    number["scale"] = 0.75

# setup a sequence that will count down for us
countdownSeq = Sequence(
    Func(setNumber, 3),
    inter,
    Func(setNumber, 2),
    inter,
    Func(setNumber, 1),
    inter,
    Func(setNumber, "GO"),
    inter)
countdownSeq.start()

while countdownSeq.isPlaying():
    # run as long as the sequence plays
    taskMgr.step()
# stop application after the sequence is done
