from direct.showbase.ShowBase import ShowBase
from panda3d.core import Vec3, Point3
from direct.gui.DirectGui import DirectFrame
from direct.gui.DirectGui import DirectLabel
from direct.gui import DirectGuiGlobals as DGG

class DragAndDrop(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        # accept the esc button to close the application
        self.accept("escape", exit)

        self.dropAreas = []
        self.ratio = float(base.win.getXSize()) / float(base.win.getYSize())

        self.dropAreaA = DirectFrame(
            text = "Drop Area A",
            text_scale = 0.15,
            text_pos = (0, 0.75, 0),
            text_bg = (1,1,1,1),
            # make the frame occupy the whole window
            frameSize = (-0.5, 0.5, -0.5, 0.5),
            borderWidth = (0.0, 0.0),
            # set the frames color to white
            frameColor = (1, 1, 1, 1),
            pos = (0.7, 0, 0))
        self.dropAreaA.reparentTo(base.a2dLeftCenter)
        self.dropAreas.append(self.dropAreaA)

        self.dropAreaB = DirectFrame(
            text = "Drop Area B",
            text_scale = 0.15,
            text_pos = (0, 0.30, 0),
            text_bg = (1,1,1,1),
            # make the frame occupy the whole window
            frameSize = (-0.25, 0.25, -0.25, 0.25),
            borderWidth = (0.0, 0.0),
            # set the frames color to white
            frameColor = (0, 1, 0, 1),
            pos = (-0.7, 0, -0.45))
        self.dropAreaB.reparentTo(base.a2dTopRight)
        self.dropAreas.append(self.dropAreaB)

        self.dropAreaC = DirectFrame(
            text = "Drop Area C",
            text_scale = 0.15,
            text_pos = (0, 0.30, 0),
            text_bg = (1,1,1,1),
            # make the frame occupy the whole window
            frameSize = (-0.25, 0.25, -0.25, 0.25),
            borderWidth = (0.0, 0.0),
            # set the frames color to white
            frameColor = (0, 0, 1, 1),
            pos = (-0.7, 0, 0.45))
        self.dropAreaC.reparentTo(base.a2dBottomRight)
        self.dropAreas.append(self.dropAreaC)

        pos = self.dropAreaA.getPos(render)
        self.dragable = DirectLabel(
            text = "Drag Me!",
            text_fg = (1,1,1,1),
            text_scale = 0.05,
            frameSize = (-0.15, 0.15, -0.15, 0.15),
            frameColor = (1, 0, 0, 1),
            borderWidth = (0.05, 0.05),
            pos = (pos[0] * self.ratio, 0, pos[1]),
            state = DGG.NORMAL,
            relief = DGG.RAISED)
        self.dragable.setTransparency(True)
        self.dragable["activeState"] = 0
        self.dragable.bind(DGG.B1PRESS, self.dragStart, [self.dragable])
        self.dragable.bind(DGG.B1RELEASE, self.dragStop)
        self.dragable.bind(DGG.ENTER, self.inObject, [self.dragable])
        self.dragable.bind(DGG.EXIT, self.outObject, [self.dragable])

    def inObject(self, element, event):
        # Can be used to highlight objects
        print ("over object", element)

    def outObject(self, element, event):
        # Can be used to unhighlight objects
        pass

    def dragStart(self, element, event):
        print ("start drag")
        taskMgr.remove('dragDropTask')
        vWidget2render2d = element.getPos(render2d)
        vMouse2render2d = Point3(event.getMouse()[0], 0, event.getMouse()[1])
        editVec = Vec3(vWidget2render2d - vMouse2render2d)
        t = taskMgr.add(self.dragTask, 'dragDropTask')
        t.element = element
        t.editVec = editVec
        t.elementStartPos = element.getPos()

    def dragTask(self, task):
        mwn = base.mouseWatcherNode
        if mwn.hasMouse():
            vMouse2render2d = Point3(mwn.getMouse()[0], 0, mwn.getMouse()[1])
            newPos = vMouse2render2d + task.editVec
            task.element.setPos(render2d, newPos)
        return task.cont

    def dragStop(self, event):
        print ("stop drag with:", event)
        isInArea = False

        t = taskMgr.getTasksNamed('dragDropTask')[0]
        for dropArea in self.dropAreas:
            if self.isInBounds(event.getMouse(), dropArea["frameSize"], dropArea.getPos(render)):
                print ("inside Area:", dropArea["text"], dropArea.getPos(render))
                isInArea = True
                t.element.setPos(dropArea.getPos(render))
                t.element.setX(t.element.getX() * self.ratio)
                #t.element.setX(t.element.getX() - t.element.getWidth() / 2.0)
                break

        if not isInArea:
            t.element.setPos(t.elementStartPos)
        taskMgr.remove('dragDropTask')

    def isInBounds(self, location, bounds, posShift=None):
        x = 0 if posShift is None else posShift.getX()
        y = 0 if posShift is None else posShift.getZ()

        left = x + bounds[0]
        right = x + bounds[1]
        bottom = y + bounds[2]
        top = y + bounds[3]

        # are we outside of the bounding box from the left
        if location[0] < left: return False
        # are we outside of the bounding box from the right
        if location[0] > right: return False
        # are we outside of the bounding box from the bottom
        if location[1] < bottom: return False
        # are we outside of the bounding box from the top
        if location[1] > top: return False
        # the location is inside the bbox
        return True

APP = DragAndDrop()
APP.run()
