"""here we will show, how to simply change the mouse cursor in the game"""
from direct.showbase.ShowBase import ShowBase
base = ShowBase()
import os, sys
from panda3d.core import WindowProperties

baseDir = os.path.join("..", "data", "cursor")
x11Cur = os.path.join(baseDir, "cursor.x11")
winCur = os.path.join(baseDir, "cursor.cur")

base.win.clearRejectedProperties()
props = WindowProperties()
# check for the os and set the specific cursor file
if sys.platform.startswith('linux'):
	props.setCursorFilename(x11Cur)
else:
	props.setCursorFilename(winCur)
base.win.requestProperties(props)

base.run()
