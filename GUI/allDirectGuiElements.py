#!/usr/bin/env python
'''
Demonstrate all the various directGUI elements
'''

# DirectGUI imports
from direct.gui import DirectGuiGlobals as DGG
from direct.gui.DirectButton import DirectButton
from direct.gui.DirectCheckBox import DirectCheckBox
from direct.gui.DirectCheckButton import DirectCheckButton
from direct.gui.DirectDialog import (
    OkDialog,
    OkCancelDialog,
    YesNoDialog,
    YesNoCancelDialog,
    RetryCancelDialog)
from direct.gui.DirectEntry import DirectEntry
from direct.gui.DirectEntryScroll import DirectEntryScroll
from direct.gui.DirectFrame import DirectFrame
from direct.gui.DirectLabel import DirectLabel
from direct.gui.DirectOptionMenu import DirectOptionMenu
from direct.gui.DirectRadioButton import DirectRadioButton
from direct.gui.DirectScrollBar import DirectScrollBar
from direct.gui.DirectScrolledFrame import DirectScrolledFrame
from direct.gui.DirectScrolledList import (
    DirectScrolledList,
    DirectScrolledListItem)
from direct.gui.DirectSlider import DirectSlider
from direct.gui.DirectWaitBar import DirectWaitBar

from panda3d.core import (
    TextNode,
    PGFrameStyle)
from direct.interval.LerpInterval import LerpFunc
from direct.showbase.ShowBase import ShowBase
from direct.fsm.FSM import FSM

assets_dir = "../data/gui/"

#
# Main class to combine the GUI elements
#
class App(ShowBase, FSM):
    def __init__(self):
        ShowBase.__init__(self)
        FSM.__init__(self, "FSM-RightFrameContent")

        self.accept("escape", exit)

        # set a custom font
        DGG.setDefaultFont(loader.loadFont(assets_dir + "Ubuntu-R.ttf"))
        # make the font look nice at a big scale
        DGG.getDefaultFont().setPixelsPerUnit(100)

        # Delay initial setup by 0.5s to let the window set it's final
        # size and we'll be able to use the screen corner/edge variables
        taskMgr.doMethodLater(0.5, self.setup, "delayed setup", extraArgs = [])

    def setup(self):
        """
        Main setup function that will call all other setup functions and
        creates the two (red and green) main frames where we will add
        all the other gui elements to.
        """
        # get the screen width in Panda3D units. A square window should
        # be -1, 1 left right and -1, 1 bottom and top.
        # A window with a ratio of 16:9 will have -1.777 left
        # and 1.777 units to the right edge and a 4:3 would be
        # -1.33 to 1.33.
        self.screenWidth = abs(base.a2dRight) + abs(base.a2dLeft)
        self.leftEdge = -(self.screenWidth * (2.0 / 3.0))
        self.rightEdge = self.screenWidth * (1.0 / 3.0)

        # create a frame wich is attached to the left edge of the screen
        # it's width is 1/3 of the screen size and it's zero point is at
        # the center of the left edge
        self.leftFrame = DirectFrame(
            frameColor=(0.85, 0.85, 0.85, 1),
            frameSize=(0, self.rightEdge, base.a2dBottom, base.a2dTop))
        self.leftFrame.reparentTo(base.a2dLeftCenter)

        # create a frame which is attached to the right edge of the
        # screen and moved to the left by half the width of the frame to
        # have it's local 0 point directly in the center of the frame.
        self.rightFrame = DirectFrame(
            frameColor=(0.95, 0.95, 0.95, 1),
            pos=(self.leftEdge / 2.0, 0, 0),
            frameSize=(-self.leftEdge / 2.0,self.leftEdge / 2.0,
                base.a2dBottom,base.a2dTop))
        self.rightFrame.reparentTo(base.a2dRightCenter)

        # create the left frame which will hold the radio buttons that
        # we use to switch between the content of the right frame.
        self.leftFrameCenter = self.screenWidth * (1.0 / 6.0)
        self.lblMain = DirectLabel(
            text="(DirectRadioButton)\nDirect-",
            pos=(self.leftFrameCenter, 0, base.a2dTop - 0.15),
            scale=0.075,
            frameColor=(0, 0, 0, 0))
        self.lblMain.setTransparency(True)
        self.lblMain.reparentTo(self.leftFrame)

        # set up right frame content
        self.buttonFrame = DirectButtonSample(self.rightFrame, self.createRightFrameTitle)
        self.checkBoxFrame = DirectCheckBoxSample(self.rightFrame, self.createRightFrameTitle)
        self.checkButtonFrame = DirectCheckButtonSample(self.rightFrame, self.createRightFrameTitle)
        self.dialogFrame = DirectDialogSample(self.rightFrame, self.createRightFrameTitle)
        self.entryFrame = DirectEntrySample(self.rightFrame, self.createRightFrameTitle)
        self.optionMenuFrame = DirectOptionMenuSample(self.rightFrame, self.createRightFrameTitle)
        self.scrollbarFrame = DirectScrollBarSample(self.rightFrame, self.createRightFrameTitle)
        self.sliderFrame = DirectSliderSample(self.rightFrame, self.createRightFrameTitle)
        self.waitBarFrame = DirectWaitBarSample(self.rightFrame, self.createRightFrameTitle)

        # set up left frame content
        self.radioButtonFrame = DirectRadioButtonSample(self.leftFrame, 0.15, self.request)

    def createRightFrameTitle(self, text):
        """This function will create a title with default values for the
        content of the right frame"""
        lbl = DirectLabel(
            text=text,
            pos=(0, 0, base.a2dTop - 0.25),
            scale=0.15,
            frameColor=(0, 0, 0, 0))
        lbl.setTransparency(True)
        lbl.reparentTo(self.rightFrame)
        return lbl

    def enterShowButtons(self):
        self.buttonFrame.show()
    def exitShowButtons(self):
        self.buttonFrame.hide()

    def enterShowCheckBox(self):
        self.checkBoxFrame.show()
    def exitShowCheckBox(self):
        self.checkBoxFrame.hide()

    def enterShowCheckButtons(self):
        self.checkButtonFrame.show()
    def exitShowCheckButtons(self):
        self.checkButtonFrame.hide()

    def enterShowDialogs(self):
        self.dialogFrame.show()
    def exitShowDialogs(self):
        self.dialogFrame.hide()

    def enterShowEntries(self):
        self.entryFrame.show()
    def exitShowEntries(self):
        self.entryFrame.hide()

    def enterShowOptionMenu(self):
        self.optionMenuFrame.show()
    def exitShowOptionMenu(self):
        self.optionMenuFrame.hide()

    def enterShowScrollbars(self):
        self.scrollbarFrame.show()
    def exitShowScrollbars(self):
        self.scrollbarFrame.hide()

    def enterShowSlider(self):
        self.sliderFrame.show()
    def exitShowSlider(self):
        self.sliderFrame.hide()

    def enterShowWaitBar(self):
        self.waitBarFrame.show()
    def exitShowWaitBar(self):
        self.waitBarFrame.hide()

#
# DirectGUI RadioButton sample
#
class DirectRadioButtonSample():
    def __init__(self, leftFrame, xPos, requestFunc):
        # Add radio buttons on the left frame to select the content
        # of the right frame
        # We will place the buttons directly below each other hence we
        # have to know the height of a button which is 1 plus a border
        # of 0.1 and if signs like p or y are used in the text
        # additional to high letters like l or uppercase letters an
        # extra 0.1. As we scale the button down to 0.1 1 gets 0.1
        # and the border becomes 0.01. With that we can calculate the
        # positions of each button and simply add an extra amount to
        # have them slightly offset to each other.
        self.value=[0]
        def createRadioButton(text, yPos, v, arg):
            return DirectRadioButton(
                text=text,
                text_align=TextNode.ALeft,
                scale=0.10,
                pos=(xPos, 0, yPos),
                relief=None,
                variable=self.value,
                value=[v],
                command=requestFunc,
                extraArgs=[arg],
                parent=leftFrame)
        # create all the options we have in the left frame
        options = [
            createRadioButton('Button', 0.6, 0, "ShowButtons"),
            createRadioButton('CheckBox', 0.45, 1, "ShowCheckBox"),
            createRadioButton('CheckButton', 0.3, 2, "ShowCheckButtons"),
            createRadioButton('Dialog', 0.15, 3, "ShowDialogs"),
            createRadioButton('Entry', 0, 4, "ShowEntries"),
            createRadioButton('OptionMenu', -0.15, 5, "ShowOptionMenu"),
            createRadioButton('Scroll*', -0.3, 6, "ShowScrollbars"),
            createRadioButton('Slider', -0.45, 6, "ShowSlider"),
            createRadioButton('WaitBar', -0.6, 6, "ShowWaitBar")
        ]
        # tell the radio buttons which other radio buttons are in the
        # same group
        for button in options:
            button.setOthers(options)
            button.setTransparency(True)

#
# DirectGUI Button sample
#
class DirectButtonSample():
    def __init__(self, rightFrame, createRightFrameTitle):
        # setup a title for the frame
        self.lblBtn = createRightFrameTitle("DirectButton")
        self.lblBtn.hide()

        # set up a sample button
        self.btn = DirectButton(
            text=("Normal", "Click", "Over", "Disabled"),
            frameColor=(
                (0.8, 0.8, 0.8, 1), (0, 1, 0, 1),
                (1, 1, 0, 1), (0.4, 0.4, 0.4, 1)),
            pos=(0, 0, 0.25),
            scale=0.25)
        self.btn.reparentTo(rightFrame)
        self.btn.hide()

        def disableOtherButton():
            """This function will toggle the state of self.btn between
            disabled and normal"""
            if self.btn["state"] != DGG.DISABLED:
                self.btn["state"] = DGG.DISABLED


                # Set frame to new dimensions
                bounds = self.btn.getBounds(3)
                border = (0, 0)
                # check if the button has a border
                if ((self.btn.getFrameType() != PGFrameStyle.TNone) and
                    (self.btn.getFrameType() != PGFrameStyle.TFlat)):
                    border = self.btn["borderWidth"]
                # calculate the new frame size
                self.btn["frameSize"] = (
                    bounds[0] - border[0],
                    bounds[1] + border[0],
                    bounds[2] - border[1],
                    bounds[3] + border[1])
                self.btnDisable["text"] = "Enable"
            else:
                self.btn["state"] = DGG.NORMAL
                self.btnDisable["text"] = "Disable"
                # we need to set the frameSize to None for the
                # resetFrameSize function to work properly
                self.btn["frameSize"] = None
                self.btn.resetFrameSize()

        # set up a button to disable the first sample button
        self.btnDisable = DirectButton(
            text=("Disable"),
            pos=(0, 0, -0.15),
            scale=0.25,
            command=disableOtherButton)
        self.btnDisable.reparentTo(rightFrame)
        self.btnDisable.hide()

        # Create another button which will have the smiley model as its
        # geometry and print out a given text on click. This button also
        # have sounds attached to its rollover and click state
        def printText(text):
            print(text)
        smiley = loader.loadModel("smiley")
        smiley.setR(45)
        # Now, due to the fact that button geometry  will be rendered
        # with render2d, which has z-buffering deactivated by default,
        # we need to explicitely enable it the models we want to apply
        # to the button or any other DirectGUI element.
        smiley.setDepthWrite(True)
        smiley.setDepthTest(True)
        self.btnSmiley = DirectButton(
            text="Click me",
            text_pos=(0, -1.5),
            geom=smiley,
            frameColor=(0, 0, 0, 0),
            pos=(0, 0, -0.5),
            scale=0.15,
            command=printText,
            extraArgs=["I'm a smiley"],
            rolloverSound=loader.loadSfx("audio/sfx/GUI_rollover.wav"),
            clickSound=loader.loadSfx("audio/sfx/GUI_click.wav"))
        self.btnSmiley.setTransparency(True)
        self.btnSmiley.reparentTo(rightFrame)
        self.btnSmiley.hide()

    def show(self):
        self.lblBtn.show()
        self.btn.show()
        self.btnDisable.show()
        self.btnSmiley.show()

    def hide(self):
        self.lblBtn.hide()
        self.btn.hide()
        self.btnDisable.hide()
        self.btnSmiley.hide()

#
# DirectGUI CheckBox sample
#
class DirectCheckBoxSample():
    def __init__(self, rightFrame, createRightFrameTitle):
        # setup a title for the frame
        self.lblCbx = createRightFrameTitle("DirectCheckBox")
        self.lblCbx.hide()

        # create a simple check box wich will toggle between the
        # minus and plus image. Note to get the state of the checkbox
        # use self.cbx.isChecked() and set a command if you want to know
        # when the check state is changed.
        self.cbx = DirectCheckBox(
            scale=0.05,
            pos=(0, 0, 0),
            pad=(0, 0),
            borderWidth=(0, 0),
            frameColor=(0, 0, 0, 0),
            image=assets_dir + "minusnode.png",
            uncheckedImage=assets_dir + "minusnode.png",
            checkedImage=assets_dir + "plusnode.png")
        self.cbx.setTransparency(True)
        self.cbx.reparentTo(rightFrame)
        self.cbx.hide()

    def show(self):
        self.cbx.show()
        self.lblCbx.show()

    def hide(self):
        self.cbx.hide()
        self.lblCbx.hide()

#
# DirectGUI CheckButton sample
#
class DirectCheckButtonSample():
    def __init__(self, rightFrame, createRightFrameTitle):
        # setup a title for the frame
        self.lblCbtn = createRightFrameTitle("DirectCheckButton")
        self.lblCbtn.hide()

        def printStatus(value):
            print("CheckButton new value:", value)

        # Create two check buttons that can separately be checked and
        # unchecked unlike radio buttons that have been grouped.
        self.cbtn1 = DirectCheckButton(
            text="Check Me!",
            pos=(0, 0, 0.1),
            scale=0.15,
            command=printStatus)
        self.cbtn1.reparentTo(rightFrame)
        self.cbtn1.hide()

        # create the second check button that is already checked
        self.cbtn2 = DirectCheckButton(
            text="Uncheck Me!",
            pos=(0, 0, -0.1),
            scale=0.15,
            indicatorValue=1,
            command=printStatus)
        self.cbtn2.reparentTo(rightFrame)
        self.cbtn2.hide()

    def show(self):
        self.cbtn1.show()
        self.cbtn2.show()
        self.lblCbtn.show()

    def hide(self):
        self.cbtn1.hide()
        self.cbtn2.hide()
        self.lblCbtn.hide()

#
# DirectGUI Dialog sample
#
class DirectDialogSample():
    def __init__(self, rightFrame, createRightFrameTitle):
        # setup a title for the frame
        self.lblDlg = createRightFrameTitle("DirectDialog")
        self.lblDlg.hide()

        def printResult(result, dialog):
            """Print the result of a dialog"""
            print("Dialog result:", result)
            dialog.hide()

        def createDlgOpenButton(text, zPos, dlg):
            """A helper function that creates a button that we will use
            to open one of the dialogs"""
            btn = DirectButton(
                text=text,
                scale=0.15,
                pos=(0, 0, zPos),
                command=dlg.show)
            btn.reparentTo(rightFrame)
            btn.hide()
            return btn

        # set a default geometry for our dialogs
        dialogGeom = loader.loadModel(assets_dir + "dialog")
        # set the new dialog background as default for all new dialogs
        DGG.setDefaultDialogGeom(dialogGeom)

        # create OK Dialog
        self.dlg1 = OkDialog(
            text="OK Dialog Example",
            command=printResult)
        self.dlg1["extraArgs"] = [self.dlg1]
        self.dlg1.hide()
        self.dlg1Btn = createDlgOpenButton("OK Dialog", 0.5, self.dlg1)

        # create OK Cancel Dialog
        self.dlg2 = OkCancelDialog(text="OK Cancel Dialog Example", command=printResult)
        self.dlg2["extraArgs"] = [self.dlg2]
        self.dlg2.hide()
        self.dlg2Btn = createDlgOpenButton("OK-Cancel Dialog", 0.3, self.dlg2)

        # create Yes No Dialog
        self.dlg3 = YesNoDialog(text="Yes No Dialog Example", command=printResult)
        self.dlg3["extraArgs"] = [self.dlg3]
        self.dlg3.hide()
        self.dlg3Btn = createDlgOpenButton("Yes-No Dialog", 0.1, self.dlg3)

        # create Yes No Cancel Dialog
        self.dlg4 = YesNoCancelDialog(text="Yes No Cancel Dialog Example", command=printResult)
        self.dlg4["extraArgs"] = [self.dlg4]
        self.dlg4.hide()
        self.dlg4Btn = createDlgOpenButton("Yes-No-Cancel Dialog", -0.1, self.dlg4)

        # create Retry Cancel Dialog
        self.dlg5 = RetryCancelDialog(text="Retry Cancel Dialog Example", command=printResult)
        self.dlg5["extraArgs"] = [self.dlg5]
        self.dlg5.hide()
        self.dlg5Btn = createDlgOpenButton("Retry-Cancel Dialog", -0.3, self.dlg5)

    def show(self):
        self.lblDlg.show()
        self.dlg1Btn.show()
        self.dlg2Btn.show()
        self.dlg3Btn.show()
        self.dlg4Btn.show()
        self.dlg5Btn.show()

    def hide(self):
        self.lblDlg.hide()
        self.dlg1Btn.hide()
        self.dlg2Btn.hide()
        self.dlg3Btn.hide()
        self.dlg4Btn.hide()
        self.dlg5Btn.hide()

#
# DirectGUI Entries sample
#
class DirectEntrySample():
    def __init__(self, rightFrame, createRightFrameTitle):
        # setup a title for the frame
        self.lblTxt = createRightFrameTitle("DirectEntry")
        self.lblTxt.hide()

        def cleanEntry(entry):
            """Helper function to clear the content of a DirectEntry"""
            entry.enterText("")

        # set up a simple entry field
        self.txt = DirectEntry(
            pos=(0, 0, 0.5),
            scale=0.15,
            initialText="Enter some Text")
        self.txt["focusInCommand"] = cleanEntry
        self.txt["focusInExtraArgs"] = [self.txt]
        self.txt.reparentTo(rightFrame)
        self.txt.setX(-self.txt.getBounds()[1] * 0.15 / 2.0)
        self.txt.hide()

        # set up an obscured textfield, which can for example be used
        # for password entries
        self.txtPw = DirectEntry(
            pos=(-1, 0, 0.2),
            scale=0.15,
            width=4,
            initialText="Password",
            obscured=True)
        self.txtPw["focusInCommand"] = cleanEntry
        self.txtPw["focusInExtraArgs"] = [self.txtPw]
        self.txtPw.setX(-self.txtPw.getBounds()[1]*0.15/2.0)
        self.txtPw.reparentTo(rightFrame)
        self.txtPw.hide()

        # create a textfield that automatically capitalize the first
        # sign of each word.
        self.txtAutoCap = DirectEntry(
            pos=(0, 0, -0.1),
            scale=0.15,
            autoCapitalize=True,
            initialText="Auto Capitalize")
        self.txtAutoCap["focusInCommand"] = cleanEntry
        self.txtAutoCap["focusInExtraArgs"] = [self.txtAutoCap]
        self.txtAutoCap.reparentTo(rightFrame)
        self.txtAutoCap.setX(-self.txtAutoCap.getBounds()[1]*0.15/2.0)
        self.txtAutoCap.hide()

        # create a multiline textfield
        self.txtBig = DirectEntry(
            text_scale=0.05,
            numLines=11,
            width=20,
            frameSize=(0, 10.0, -10.0, 0.036),
            initialText="Multiline text\nTest test test")
        # pack the multiline textfield in a entry scroll
        self.txtMultiline = DirectEntryScroll(
            self.txtBig,
            clipSize=(0, 1.0, -0.5, 0.036),
            pos=(0, 0, -0.4))
        self.txtMultiline.setX(-1.0/2.0)
        self.txtMultiline.reparentTo(rightFrame)
        self.txtMultiline.hide()

    def show(self):
        self.lblTxt.show()
        self.txt.show()
        self.txtPw.show()
        self.txtAutoCap.show()
        self.txtMultiline.show()

    def hide(self):
        self.lblTxt.hide()
        self.txt.hide()
        self.txtPw.hide()
        self.txtAutoCap.hide()
        self.txtMultiline.hide()

#
# DirectGUI OptionMenu sample
#
class DirectOptionMenuSample():
    def __init__(self, rightFrame, createRightFrameTitle):
        # setup a title for the frame
        self.lblMnu = createRightFrameTitle("DirectOptionMenu")
        self.lblMnu.hide()

        def showSelectedItem(item):
            """Helper function that will show the item that got selected
            from the option menu"""
            text = "Selected item: %s" % item
            self.lblChoice["text"] = text
            self.lblChoice.resetFrameSize()

        # set up a label that will show the selection
        self.lblChoice = DirectLabel(
            text="Selected item: 2. Item",
            scale=0.15,
            pos=(0, 0, -0.25))
        self.lblChoice.reparentTo(rightFrame)
        self.lblChoice.hide()

        # create the options for the menu
        options = []
        for i in range(10):
            options.append("%d. Item"%(i+1))

        # create the options menu with the second item initially
        # selected
        self.mnu = DirectOptionMenu(
            scale=0.15,
            items=options,
            initialitem="2. Item",
            highlightColor=(0,1,1,1),
            highlightScale=(1.2, 1.2),
            command=showSelectedItem)
        self.mnu.setX(-self.mnu.getBounds()[1]*0.15/2.0)
        self.mnu.reparentTo(rightFrame)
        self.mnu.hide()

    def show(self):
        self.lblMnu.show()
        self.lblChoice.show()
        self.mnu.show()

    def hide(self):
        self.lblMnu.hide()
        self.lblChoice.hide()
        self.mnu.hide()

#
#  DirectGUI Scrollbars sample
#
class DirectScrollBarSample():
    def __init__(self, rightFrame, createRightFrameTitle):
        # setup a title for the frame
        self.lblScroll = createRightFrameTitle(
            "DirectScroll[ed]\n-Bar/-Frame/-List")
        self.lblScroll.hide()

        def updatePage():
            """Sample function that shows how to get the value of a
            scrollbar which then can be used for scrolling"""
            self.lblPagePercent["text"] = "%d%%"%self.scrollBar.getValue()
            self.lblPagePercent.resetFrameSize()

        self.lblPagePercent = DirectLabel(
            text="25%",
            text_align=TextNode.ALeft,
            scale=0.15,
            pos=(-0.8,0,0))
        self.lblPagePercent.reparentTo(rightFrame)
        self.lblPagePercent.hide()

        #
        # DirectScrollBar
        #
        self.scrollBar = DirectScrollBar(
            pos=(-0.84,0,0),
            range=(0,100),
            value=25,
            scrollSize=1,
            pageSize=10,
            orientation=DGG.VERTICAL,
            command=updatePage)
        self.scrollBar.reparentTo(rightFrame)
        self.scrollBar.hide()

        #
        # DirectScrolledFrame
        #
        self.frmScroll = DirectScrolledFrame(
            frameSize=(-0.3, 0.3, -0.5, 0.5),
            canvasSize=(-1, 1, -1, 1),
            scrollBarWidth=0.04,
            pos=(-0.2,0,0))
        self.frmScroll.reparentTo(rightFrame)
        self.frmScroll.hide()
        self.testImage = DirectFrame(
            image="maps/envir-ground.jpg",
            parent=self.frmScroll.getCanvas())

        #
        # DirectScrolledList
        #
        self.lstScroll = DirectScrolledList(
            text = "Scrolled List Example",
            text_scale= 0.05,
            text_pos = (0, 0.3),
            pos = (0.55, 0, 0),
            relief = None,
            # Use the default dialog box image as the background
            image=DGG.getDefaultDialogGeom(),
            # Scale it to fit around everyting
            image_scale = (0.7, 1, .8),
            incButton_text="Increment",
            incButton_relief=DGG.RAISED,
            incButton_pos=(0.0, 0.0, -0.36),
            incButton_scale=0.1,
            # Same for the decrement button
            decButton_text="Decrement",
            decButton_relief=DGG.RAISED,
            decButton_pos=(0.0, 0.0, 0.175),
            decButton_scale=0.1,
            # each item is a button with text on it
            numItemsVisible=4,
            # itemFrame is a DirectFrame
            # Use it to scale up or down the items and to place it relative
            # to eveything else
            itemFrame_pos=(0, 0, 0.06),
            itemFrame_scale=0.1,
            itemFrame_frameSize=(-3.1, 3.1, -3.3, 0.8),
            itemFrame_relief=DGG.GROOVE)
        self.lstScroll.reparentTo(rightFrame)
        self.lstScroll.hide()

        # Setup DirectScrolledListItems
        def printItem(item):
            print(item)
            print(self.lstScroll.getSelectedIndex())
            print(self.lstScroll.getSelectedText())
        setEven = False
        # color for even lines
        even = (0.5, 0.5, 0.5, 1)
        # color for odd lines
        odd = (0.6, 0.6, 0.6, 1)
        for elem in ["Element 1", "Element 2", "Element 3", "Element 4", "Element 5", "Element 6"]:
            if setEven:
                colors = (even, even, even, (0,0,1,1))
            else:
                colors = (odd, odd, odd, (0,0,1,1))
            setEven = not setEven
            # set up the item
            itm = DirectScrolledListItem(
                text=elem,
                frameColor=colors,
                frameSize=(-3.05,3.05,-0.25,0.75),
                parent=self.lstScroll,
                command=printItem,
                borderWidth=(0,0),
                relief=DGG.FLAT,
                extraArgs=[elem])
            # make sure the items size got set correct
            itm.resetFrameSize()
            # add the item to the list without refreshing it
            self.lstScroll.addItem(itm, False)
        # refresh the list after all items are added to make the items
        # become visible
        self.lstScroll.refresh()

    def show(self):
        self.lblScroll.show()
        self.lblPagePercent.show()
        self.scrollBar.show()
        self.frmScroll.show()
        self.lstScroll.show()

    def hide(self):
        self.lblScroll.hide()
        self.lblPagePercent.hide()
        self.scrollBar.hide()
        self.frmScroll.hide()
        self.lstScroll.hide()

#
# DirectGUI Slider sample
#
class DirectSliderSample():
    def __init__(self, rightFrame, createRightFrameTitle):
        # setup a title for the frame
        self.lblSlider = createRightFrameTitle("DirectSlider")
        self.lblSlider.hide()

        # a simple slider
        self.slider1 = DirectSlider(
            text="DirectSlider",
            text_scale=0.1,
            pos=(-0.4, 0, 0),
            frameSize=(-0.5, 0.5, -0.04, 0.04))
        self.slider1.reparentTo(rightFrame)
        self.slider1.hide()

        def updateSliderText():
            self.slider2["text"] = "%d%%"%self.slider2.getValue()
        # a more configured slider with vertical alignment.
        # Note, the line of the slider is the main frame and the tumb,
        # which is basically a DirectButton can be accessed via thumb_*
        self.slider2 = DirectSlider(
            text="50%",
            text_scale=0.25,
            text_pos=(0.15, 0),
            text_align=TextNode.ALeft,
            scale=0.5,
            pos=(0.5, 0, 0),
            orientation=DGG.VERTICAL,
            range=(0, 100),
            value=50,
            scrollSize=1,
            pageSize=10,
            thumb_frameColor=(1, 0, 0, 1),
            command=updateSliderText)
        self.slider2.reparentTo(rightFrame)
        self.slider2.hide()

    def show(self):
        self.lblSlider.show()
        self.slider1.show()
        self.slider2.show()

    def hide(self):
        self.lblSlider.hide()
        self.slider1.hide()
        self.slider2.hide()

#
# DirectGUI WaitBar sample
#
class DirectWaitBarSample():
    def __init__(self, rightFrame, createRightFrameTitle):
        # setup a title for the frame
        self.lblWb = createRightFrameTitle("DirectWaitBar")
        self.lblWb.hide()

        def updateWaitBar1(value):
            self.wb1["text"] = "%d%%"%value
            self.wb1.update(value)
        # set up a simply collored waitbar
        self.wb1 = DirectWaitBar(
            text="0%",
            text_fg=(1, 1, 1, 1),
            value=0,
            scale=0.75,
            pos=(0, 0, 0),
            frameColor=(0.5, 0.5, 0.5, 1),
            barColor=(0.2, 0.2, 0.2, 1),
            barRelief=DGG.RAISED,
            borderWidth=(0.01, 0.01))
        self.wb1.reparentTo(rightFrame)
        self.wb1.hide()

        # add a lerp function to automatically change the bars value
        self.wb1Lerp = LerpFunc(
            updateWaitBar1,
            fromData=0,
            toData=100,
            duration=3.0)


        def updateWaitBar2(value):
            self.wb2["text"] = "%d%%"%value
            self.wb2.update(value)
        # set up a textured waitbar
        self.wb2 = DirectWaitBar(
            text="100%",
            text_fg=(1, 1, 1, 1),
            value=100,
            scale=0.75,
            pos=(0, 0, -0.5),
            frameColor=(0.5, 0.5, 0.5, 1),
            barColor=(0.9, 0.9, 1, 1),
            barRelief=DGG.RIDGE,
            borderWidth=(0.01, 0.01),
            barTexture=loader.loadTexture("maps/envir-groundcover1.png"))
        self.wb2.reparentTo(rightFrame)
        self.wb2.hide()

        # add a lerp function to automatically change the bars value
        self.wb2Lerp = LerpFunc(
            updateWaitBar2,
            fromData=100,
            toData=0,
            duration=3.0)

    def show(self):
        self.lblWb.show()
        self.wb1.show()
        self.wb2.show()
        self.wb1Lerp.loop()
        self.wb2Lerp.loop()

    def hide(self):
        self.lblWb.hide()
        self.wb1.hide()
        self.wb2.hide()
        self.wb1Lerp.finish()
        self.wb2Lerp.finish()

app = App()
app.run()
