from direct.showbase.ShowBase import ShowBase
base = ShowBase()
from panda3d.core import CardMaker
from panda3d.core import LVecBase4f
from panda3d.core import TransparencyAttrib
from panda3d.core import NodePath
from direct.interval.LerpInterval import LerpColorScaleInterval
from direct.interval.IntervalGlobal import Sequence

from direct.gui.OnscreenText import OnscreenText

# TODO: place cm over direct gui elements (does work with aspect2d but not fullscreen...)

# setup a fullscreen card that will be on top of all the other things
# and which we are using to fade in and out
cm = CardMaker("fade")
cm.setFrameFullscreenQuad()
cmnode = NodePath(cm.generate())
cmnode.setTransparency(TransparencyAttrib.MAlpha)
cmnode.setBin("fixed", 1000)
cmnode.reparentTo(render2d)

# create some gui element that could be covered by the fader card
text = OnscreenText(
    text = "TEST123",
    scale = 0.2,
    pos = (0, 0),
    fg = (0,0,1,1))
# IMPORTANT: setBin has to be called for each element that is created after the
#            black fading quad
text.setBin("fixed", 100)

fadeInInterval = LerpColorScaleInterval(
    cmnode,
    2,
    LVecBase4f(0.0,0.0,0.0,0.0),
    LVecBase4f(0.0,0.0,0.0,1.0))

fadeOutInterval = LerpColorScaleInterval(
    cmnode,
    2,
    LVecBase4f(0.0,0.0,0.0,1.0),
    LVecBase4f(0.0,0.0,0.0,0.0))

fadeInOut = Sequence(
    fadeInInterval,
    fadeOutInterval,
    name="fadeInOut")
fadeInOut.start()

base.run()
