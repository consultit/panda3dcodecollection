"""This app will demonstrate how to create a scrolled frame which will
be scroll according to the contained text.

NOTE: best run in window mode to be able to scale the window/frame
smaller than the actual text in it."""

from direct.showbase.ShowBase import ShowBase
base = ShowBase()
from direct.gui.DirectGui import DirectScrolledFrame, DGG
from direct.gui.OnscreenText import OnscreenText
from direct.showbase.DirectObject import DirectObject
from panda3d.core import TextNode

# Enhance font readability
DGG.getDefaultFont().setPixelsPerUnit(75)

# Some long text we can use for testing
testtext = """Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula
ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor.
Praesent et diam eget libero egestas mattis sit amet vitae augue.
Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut
libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit
amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis
felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor
nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit
pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna
ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam
pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et
magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan
ultricies. Mauris vitae nisi at sem facilisis semper ac in est."""

# create a frame that will create the scrollbars for us
scrollFrame = DirectScrolledFrame(
    # make the frame occupy the whole window
    frameSize = (0, base.a2dRight * 2, base.a2dBottom * 2, 0),
    # make the canvas as big as the frame
    canvasSize = (0, base.a2dRight * 2, base.a2dBottom * 2, 0),
    # set the frames color to white
    frameColor = (1, 1, 1, 1))
# reparent the frame to the top left corner of our window
scrollFrame.reparentTo(base.a2dTopLeft)

# create the text field that will be put inside the scrolled frame
scrollFrameText = OnscreenText(
    # the size of a sign in panda units
    scale = 0.1,
    # position the text a bit down to not cut off the first line
    pos = (0, -0.1),
    # set the text from default centered to left aligned
    align = TextNode.ALeft)
# reparent the text to the canvas of our scrollable frame
scrollFrameText.reparentTo(scrollFrame.getCanvas())

def setText(text):
    """This function will set the Text inside the frame and recalculate
    the necessary canvas size"""
    global scrollFrame
    scrollFrameText.setText(text)
    # get the size of the written text
    textbounds = scrollFrameText.getTightBounds()
    # resize the canvas. This will make the scrollbars dis-/appear,
    # dependent on if the canvas is bigger than the frame size.
    scrollFrame["canvasSize"] = (0, textbounds[1].getX(),
                                textbounds[0].getZ(), 0)
# add ou test text twice to have enough text for scroll testing
setText(testtext + "\n\n" + testtext)

def recalcAspectRatio(window):
    """get the new aspect ratio to resize the frame"""
    global scrollFrame
    # set the frame size to the window borders again
    scrollFrame["frameSize"] = (
        0, base.a2dRight * 2,
        base.a2dBottom * 2, 0)


do = DirectObject()
do.accept("window-event", recalcAspectRatio)

base.run()
