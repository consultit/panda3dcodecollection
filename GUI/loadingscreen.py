"""Show a directWaitbar to display the current loading state of the application"""

from direct.showbase.ShowBase import ShowBase
from direct.gui.DirectGui import DirectFrame
from direct.gui.DirectGui import DirectWaitBar
from direct.gui.DirectGui import DirectLabel
from direct.gui.DirectGui import DGG
from panda3d.core import TextNode


class LoadingScreen(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        # Enhance font readability
        DGG.getDefaultFont().setPixelsPerUnit(100)

        # a fill panel so the player doesn't see how everything
        # gets loaded in the background
        self.frameMain = DirectFrame(
            # size of the frame
            frameSize = (base.a2dLeft, base.a2dRight,
                         base.a2dTop, base.a2dBottom),
            # tramsparent bg color
            frameColor = (0.05, 0.1, 0.25, 1))

        # the text Loading... on top
        self.lblLoading = DirectLabel(
            scale = 0.25,
            pos = (0, 0, 0.5),
            frameColor = (0, 0, 0, 0),
            text = "Loading...",
            text_align = TextNode.ACenter,
            text_fg = (1,1,1,1))
        self.lblLoading.reparentTo(self.frameMain)

        # the waitbar on the bottom
        self.wbLoading = DirectWaitBar(
            text = "0%",
            text_fg = (1,1,1,1),
            value = 100,
            pos = (0, 0, -0.5),
            barColor = (0, 0, 1, 1))
        self.wbLoading.reparentTo(self.frameMain)

        # now show the main frame
        self.frameMain.show()
        # and render two frames so the loading screen
        # is realy shown on screen
        base.graphicsEngine.renderFrame()
        base.graphicsEngine.renderFrame()

    def setLoadingValue(self, value):
        """Set the waitbar value to the given value, where
        value has to be a integer from 0 to 100"""
        if value > 100: value = 100
        if value < 0: value = 0
        self.wbLoading["value"] = value
        self.wbLoading["text"] = "{0}%".format(value)
        base.graphicsEngine.renderFrame()
        base.graphicsEngine.renderFrame()

ls = LoadingScreen()

loaded = False
lv = 0
elapsed = 0.0
def doLoad(task):
    global lv, ls, loaded, elapsed
    elapsed += task.getDt()
    # increase the loading value four times a seconds
    if elapsed >= 0.25:
        lv += 1
        elapsed = 0.0
    ls.setLoadingValue(lv)
    if lv == 100:
        loaded = True
        return task.done
    return task.cont
taskMgr.doMethodLater(0.25, doLoad, "Loading")

ls.run()
