"""controll the GUI via keyboard and controller events"""

# WIP NOTE: THIS IS STILL IN DEVELOPMENT AND DOES NOT WORK YET!

# import the engine stuff
from direct.showbase.ShowBase import ShowBase
# import the GUI related things
from direct.gui.DirectButton import DirectButton
# sys for exiting
import sys

class GUI(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        self.btn1 = DirectButton(text="Button 1", scale=0.1, pos=(0,0,0.25),
                                 command=self.printout, extraArgs=["1"])
        self.btn2 = DirectButton(text="Button 2", scale=0.1, pos=(0,0,-0.25),
                                 command=self.printout, extraArgs=["2"])
        self.guiIdxDict = {1:[self.btn1,True], 2:[self.btn2,False]}
        self.acceptKeys()

    def printout(self, btn):
        print ("pressed btn", btn)

    def previousActive(self):
        if self.btn1.getTag("active"):
            print ("btn1 is active")

    def acceptEvents(self):
        self.accept("up", self.previousActive)

    def acceptKeys(self):
        """This function represents another input class which will
        normally handle all kinds of input like keyboard and joystick events
        and sends generic events dependent on the input"""
        self.accept("arrow_up", lambda: messenger.send("up"))
        self.accept("arrow_up", lambda: messenger.send("down"))
        self.accept("enter", lambda: messenger.send("action"))

gui = GUI()
gui.run()







