"""A simple topdown camera movable by mouse. Either by pressing and holding a
mouse key or by moving the cursor towards one of the screen edges like seen in
most topdown and isometric strategy games."""

import sys

from direct.showbase.ShowBase import ShowBase
from panda3d.core import Point2

class Runner(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        self.accept("escape", sys.exit)

        # simple level
        self.environ = loader.loadModel("environment")
        self.environ.reparentTo(self.render)

        #
        # Camera
        #
        # set ortographic lens
        #lens = OrthographicLens()
        #lens.setFilmSize(80, 60)  # Or whatever is appropriate for your scene
        #base.cam.node().setLens(lens)
        self.camLens.setFov(90)

        # camera movement
        self.mousePos = None
        self.startCameraMovement = False
        self.accept("mouse1", self.setMoveCamera, [True])
        self.accept("mouse1-up", self.setMoveCamera, [False])
        # accept zooming keys
        self.accept("wheel_up", self.zoom, [True])
        self.accept("wheel_down", self.zoom, [False])
        self.acceptOnce("+", self.zoom, [True])
        self.acceptOnce("-", self.zoom, [False])
        # disable pandas default mouse-camera controls so we can handle the cam
        # movements by ourself
        self.disableMouse()
        self.camera.setP(-90)
        # this variable will be used to determine the distance from the scene to
        # the camera
        self.camDistance = 75.0
        # min and maximal distance between scene and camera
        self.maxCamDistance = 150.0
        self.minCamDistance = 50.0
        # the speed at which the mousewheel and +/- will zoom the camera in/out
        self.zoomSpeed = 5.0
        # the speed at which the mouse moves the camera
        self.mouseSpeed = 50
        # the speed at which the camera travels if the mouse hits the screen borders
        self.camBorderSpeed = 100
        # the distance the mouse needs to be from the window edges to start scrolling the camera
        self.xEdgeStartMove = 0.85
        self.yEdgeStartMove = 0.85

        # add the cameras update task so it will be updated every frame
        self.taskMgr.add(self.updateCam, "task_camActualisation", priority=-4)

    #
    # CAMERA SPECIFIC FUNCTIONS
    #
    def setMoveCamera(self, moveCamera):
        # store the mouse position if weh have a mouse
        if base.mouseWatcherNode.hasMouse():
            x = base.mouseWatcherNode.getMouseX()
            y = base.mouseWatcherNode.getMouseY()
            self.mousePos = Point2(x, y)
        # set the variable according to if we want to move the camera or not
        self.startCameraMovement = moveCamera

    def zoom(self, zoomIn):
        if zoomIn:
            # check if we are far enough away to further zoom in
            if self.camDistance > self.minCamDistance:
                # zoom in by 1 unit
                self.camDistance -= self.zoomSpeed
                # change camera movement speed
                self.mouseSpeed -= self.zoomSpeed
            # reaccept the zoom in key
            self.acceptOnce("+", self.zoom, [True])
        else:
            # check if we are close enough to further zoom out
            if self.camDistance < self.maxCamDistance:
                # zoom out by 1 unit
                self.camDistance += self.zoomSpeed
                # change camera movement speed
                self.mouseSpeed += self.zoomSpeed
            # reaccept the zoom out key
            self.acceptOnce("-", self.zoom, [False])

    def updateCam(self, task):
        # variables to store the mouses current x and y position
        x = 0.0
        y = 0.0
        if base.mouseWatcherNode.hasMouse():
            # get the mouse position
            x = base.mouseWatcherNode.getMouseX()
            y = base.mouseWatcherNode.getMouseY()
        if base.mouseWatcherNode.hasMouse() \
        and self.mousePos is not None \
        and self.startCameraMovement:
            # Move the camera if the left mouse key is pressed and the mouse moved
            mouseMoveX = (self.mousePos.getX() - x) * (self.mouseSpeed + globalClock.getDt())
            mouseMoveY = (self.mousePos.getY() - y) * (self.mouseSpeed + globalClock.getDt())
            self.mousePos = Point2(x, y)

            self.camera.setX(self.camera, mouseMoveX)
            self.camera.setZ(self.camera, mouseMoveY)
        else:
            # Move the camera if the mouse is near a window edge
            if x >= self.xEdgeStartMove:
                velocity = (x - self.xEdgeStartMove) * 10
                self.camera.setX(self.camera, self.camBorderSpeed * velocity * globalClock.getDt())
            elif x <= -self.xEdgeStartMove:
                velocity = (-x - self.xEdgeStartMove) * 10
                self.camera.setX(self.camera, -self.camBorderSpeed * velocity * globalClock.getDt())
            if y >= self.yEdgeStartMove:
                velocity = (y - self.yEdgeStartMove) * 10
                self.camera.setZ(self.camera, self.camBorderSpeed * velocity * globalClock.getDt())
            elif y <= -self.yEdgeStartMove:
                velocity = (-y - self.yEdgeStartMove) * 10
                self.camera.setZ(self.camera, -self.camBorderSpeed * velocity * globalClock.getDt())

        # set the cameras zoom
        camera.setZ(self.camDistance)

        # continue the task until it got manually stopped
        return task.cont

runner = Runner()
runner.run()
