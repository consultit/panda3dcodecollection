import sys

from direct.showbase.ShowBase import ShowBase
from panda3d.core import Vec3, LVecBase3f
from panda3d.core import NodePath, PandaNode

class Runner(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        # accept the esc button to close the application
        self.accept("escape", sys.exit)

        # simple level
        self.environ = loader.loadModel("environment")
        self.environ.reparentTo(self.render)

        # enable movements through the level
        self.keyMap = {"left":0, "right":0, "forward":0, "backward":0}
        self.player = self.loader.loadModel("smiley")
        self.player.setPos(0, 0, 0)
        self.player.reparentTo(self.render)
        self.accept("w", self.setKey, ["forward",1])
        self.accept("w-up", self.setKey, ["forward",0])
        self.accept("a", self.setKey, ["left",1])
        self.accept("a-up", self.setKey, ["left",0])
        self.accept("s", self.setKey, ["backward",1])
        self.accept("s-up", self.setKey, ["backward",0])
        self.accept("d", self.setKey, ["right",1])
        self.accept("d-up", self.setKey, ["right",0])
        self.accept("f8", self.toggleCinematic)

        # disable pandas default mouse-camera controls so we can handle the cam
        # movements by ourself
        self.disableMouse()

        # screen sizes
        self.winXhalf = base.win.getXSize() // 2
        self.winYhalf = base.win.getYSize() // 2

        self.mouseSpeedX = 0.1
        self.mouseSpeedY = 0.1

        # Enable camera movement smoothing
        self.enableCinematic = True
        self.smoothCamera = 1.0
        self.camViewTarget = LVecBase3f()

        self.camera.setH(180)
        self.camera.reparentTo(self.player)
        self.camera.setZ(self.player, 2)

        # maximum and minimum pitch
        self.maxPitch = 90
        self.minPitch = -80

        # the players movement speed
        self.movementSpeedForward = 25
        self.movementSpeedBackward = 25
        self.striveSpeed = 25

        self.taskMgr.add(self.move, "moveTask", priority=-4)

    def toggleCinematic(self):
        self.enableCinematic = not self.enableCinematic
        print("cinematic active: ", self.enableCinematic)

    def setKey(self, key, value):
        self.keyMap[key] = value

    def move(self, task):
        if not base.mouseWatcherNode.hasMouse(): return task.cont

        pointer = base.win.getPointer(0)
        mouseX = pointer.getX()
        mouseY = pointer.getY()

        if base.win.movePointer(0, self.winXhalf, self.winYhalf):
            # calculate the looking up/down of the camera.
            # NOTE: for first person shooter, the camera here can be replaced
            # with a controlable joint of the player model
            p = 0
            if self.enableCinematic:
                # calculate the target that the camera should move to
                p = self.camViewTarget.getY() - (mouseY - self.winYhalf) * self.mouseSpeedY
            else:
                # calculate the pitch the camera should directly move to
                p = self.camera.getP() - (mouseY - self.winYhalf) * self.mouseSpeedY
            # some sanity checks
            if p < self.minPitch:
                p = self.minPitch
            elif p > self.maxPitch:
                p = self.maxPitch
            if self.enableCinematic:
                # set the target for the camera to look at
                self.camViewTarget.setY(p)
            else:
                # directly set the cameras pitch
                self.camera.setP(p)
                # actualize the target so we don't get an unexpected
                # move if we toggle between cinematic and non cinematic
                # mode
                self.camViewTarget.setY(p)

            # rotate the player's heading according to the mouse x-axis movement
            h = 0
            if self.enableCinematic:
                # use the cameraview target to calculate the new heading
                h = self.camViewTarget.getX() - (mouseX - self.winXhalf) * self.mouseSpeedX
            else:
                # directly use the players heading to calculate the new
                # one
                h = self.player.getH() - (mouseX - self.winXhalf) * self.mouseSpeedX
            # some sanity checks
            if h < -360:
                h += 360
                if self.enableCinematic:
                    # make sure the heading of the player is correct
                    self.player.setH(self.player.getH() + 360)
            elif h > 360:
                h -= 360
                if self.enableCinematic:
                    # make sure the heading of the player is correct
                    self.player.setH(self.player.getH() - 360)
            if self.enableCinematic:
                # set the target heading to smoothly rotate the player
                # in that direction later on
                self.camViewTarget.setX(h)
            else:
                self.player.setH(h)
                # actualize the target so we don't get an unexpected
                # move if we toggle between cinematic and non cinematic
                # mode
                self.camViewTarget.setX(h)

        # Cinematic camera calculations
        # calculate the new pitch for the camera
        if self.enableCinematic and self.camera.getP() != self.camViewTarget.getY():
            newP = self.camera.getP() + (self.camViewTarget.getY() - self.camera.getP()) * (self.smoothCamera / 100.0)
            self.camera.setP(newP)
        # calculate the new heading for the player
        if self.enableCinematic and self.player.getH() != self.camViewTarget.getX():
            newH = self.player.getH() + (self.camViewTarget.getX() - self.player.getH()) * (self.smoothCamera / 100.0)
            self.player.setH(newH)

        # basic movement of the player
        if self.keyMap["left"] != 0:
            self.player.setX(self.player, self.striveSpeed * globalClock.getDt())
        if self.keyMap["right"] != 0:
            self.player.setX(self.player, -self.striveSpeed * globalClock.getDt())
        if self.keyMap["forward"] != 0:
            self.player.setY(self.player, -self.movementSpeedForward * globalClock.getDt())
        if self.keyMap["backward"] != 0:
            self.player.setY(self.player, self.movementSpeedBackward * globalClock.getDt())

        return task.cont

runner = Runner()
runner.run()
