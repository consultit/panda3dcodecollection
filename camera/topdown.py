"""A simple topdown fixed on the player camera"""

import sys

from direct.showbase.ShowBase import ShowBase

class Runner(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        self.accept("escape", sys.exit)

        # simple Player setup
        self.keyMap = {"left":0, "right":0, "forward":0, "up":0, "down":0}
        self.player = self.loader.loadModel("smiley")
        self.player.setPos(0, 10, 1)
        self.player.reparentTo(self.render)
        self.accept("arrow_left", self.setKey, ["left",1])
        self.accept("arrow_right", self.setKey, ["right",1])
        self.accept("arrow_up", self.setKey, ["forward",1])
        self.accept("arrow_left-up", self.setKey, ["left",0])
        self.accept("arrow_right-up", self.setKey, ["right",0])
        self.accept("arrow_up-up", self.setKey, ["forward",0])
        self.accept("w", self.setKey, ["up",1])
        self.accept("w-up", self.setKey, ["up",0])
        self.accept("s", self.setKey, ["down",1])
        self.accept("s-up", self.setKey, ["down",0])
        self.taskMgr.add(self.move, "moveTask")
        # simple level
        self.environ = loader.loadModel("environment")
        self.environ.reparentTo(self.render)

        #
        # Camera
        #
        # accept zooming keys
        self.acceptOnce("+", self.zoom, [True])
        self.acceptOnce("-", self.zoom, [False])
        # disable pandas default mouse-camera controls so we can handle the cam
        # movements by ourself
        self.disableMouse()
        # this variable will be used to determine the distance from the player to
        # the camera
        self.camDistance = 15.0
        # min and maximal distance between player and camera
        self.maxCamDistance = 35.0
        self.minCamDistance = 15.0

        # add the cameras update task so it will be updated every frame
        self.taskMgr.add(self.updateCam, "task_camActualisation", priority=-4)

    #
    # PLAYER SPECIFIC FUNCTIONS
    #
    def setKey(self, key, value):
        self.keyMap[key] = value

    def move(self, task):
        if self.keyMap["left"] != 0:
            self.player.setH(self.player.getH() + 300 * globalClock.getDt())
        if self.keyMap["right"] != 0:
            self.player.setH(self.player.getH() - 300 * globalClock.getDt())
        if self.keyMap["forward"] != 0:
            self.player.setY(self.player, -15 * globalClock.getDt())
        if self.keyMap["up"] != 0:
            self.player.setZ(self.player.getZ() + 15 * globalClock.getDt())
        if self.keyMap["down"] != 0:
            self.player.setZ(self.player.getZ() - 15 * globalClock.getDt())

        return task.cont

    #
    # CAMERA SPECIFIC FUNCTIONS
    #
    def zoom(self, zoomIn):
        if zoomIn:
            # check if we are far enough away to further zoom in
            if self.camDistance > self.minCamDistance:
                # zoom in by 1 unit
                self.camDistance = self.camDistance - 1
            # reaccept the zoom in key
            self.acceptOnce("+", self.zoom, [True])
        else:
            # check if we are close enough to further zoom out
            if self.camDistance < self.maxCamDistance:
                # zoom out by 1 unit
                self.camDistance = self.camDistance + 1
            # reaccept the zoom out key
            self.acceptOnce("-", self.zoom, [False])

    def updateCam(self, task):
        # simply set the camera position to the player position
        # plus the camera's set distance on the z-axis
        camera.setPos(self.player.getPos())
        camera.setZ(self.camera.getZ() + self.camDistance)
        # always look at the player
        camera.lookAt(self.player)

        # continue the task until it got manually stopped
        return task.cont

runner = Runner()
runner.run()
