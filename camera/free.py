"""A simple pivot point camera like used in Blender and other 3D applications"""

import sys

from direct.showbase.ShowBase import ShowBase
from panda3d.core import Point2

class Runner(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        self.accept("escape", sys.exit)

        # simple level
        self.environ = loader.loadModel("environment")
        self.environ.reparentTo(self.render)

        #
        # Camera
        #
        # set ortographic lens
        #lens = OrthographicLens()
        #lens.setFilmSize(80, 60)  # Or whatever is appropriate for your scene
        #base.cam.node().setLens(lens)
        self.camLens.setFov(90)

        # camera movement
        self.mousePos = None
        self.startCameraMovement = False
        self.movePivot = False
        self.accept("mouse1", self.setMoveCamera, [True])
        self.accept("mouse1-up", self.setMoveCamera, [False])
        self.accept("mouse2", self.setMoveCamera, [True])
        self.accept("mouse2-up", self.setMoveCamera, [False])
        self.accept("mouse3", self.setAndMovePivot, [True])
        self.accept("mouse3-up", self.setAndMovePivot, [False])
        self.accept("shift", self.setMovePivot, [True])
        self.accept("shift-up", self.setMovePivot, [False])
        self.accept("shift-mouse2", self.setMoveCamera, [True])
        self.accept("shift-mouse2-up", self.setMoveCamera, [False])
        # accept zooming keys
        self.accept("wheel_up", self.zoom, [True])
        self.accept("wheel_down", self.zoom, [False])
        self.acceptOnce("+", self.zoom, [True])
        self.acceptOnce("-", self.zoom, [False])

        # disable pandas default mouse-camera controls so we can handle the cam
        # movements by ourself
        self.disableMouse()
        # this variable will be used to determine the distance from the player to
        # the camera
        self.camDistance = 75.0
        # min and maximal distance between player and camera
        self.maxCamDistance = 150.0
        self.minCamDistance = 5.0
        # the speed at which the mousewheel and +/- will zoom the camera in/out
        self.zoomSpeed = 5.0
        # the speed at which the mouse moves the camera
        self.mouseSpeed = 50

        self.pivot = render.attachNewNode("Pivot Point")
        self.pivot.setPos(0, 0, 0)
        camera.reparentTo(self.pivot)

        # add the cameras update task so it will be updated every frame
        self.taskMgr.add(self.updateCam, "task_camActualisation", priority=-4)

    def setMoveCamera(self, moveCamera):
        # store the mouse position if weh have a mouse
        if base.mouseWatcherNode.hasMouse():
            x = base.mouseWatcherNode.getMouseX()
            y = base.mouseWatcherNode.getMouseY()
            self.mousePos = Point2(x, y)
        # set the variable according to if we want to move the camera or not
        self.startCameraMovement = moveCamera

    def setMovePivot(self, movePivot):
        self.movePivot = movePivot

    def setAndMovePivot(self, move):
        self.setMovePivot(move)
        self.setMoveCamera(move)

    #
    # CAMERA SPECIFIC FUNCTIONS
    #
    def zoom(self, zoomIn):
        if zoomIn:
            # check if we are far enough away to further zoom in
            if self.camDistance > self.minCamDistance:
                # zoom in by a given speed
                self.camDistance -= self.zoomSpeed
                # reaccept the zoom in key
                self.acceptOnce("+", self.zoom, [True])
        else:
            # check if we are close enough to further zoom out
            if self.camDistance < self.maxCamDistance:
                # zoom out by a given speed
                self.camDistance += self.zoomSpeed
                # reaccept the zoom out key
                self.acceptOnce("-", self.zoom, [False])

    def updateCam(self, task):
        # variables to store the mouses current x and y position
        x = 0.0
        y = 0.0
        if base.mouseWatcherNode.hasMouse():
            # get the mouse position
            x = base.mouseWatcherNode.getMouseX()
            y = base.mouseWatcherNode.getMouseY()
        if base.mouseWatcherNode.hasMouse() \
        and self.mousePos is not None \
        and self.startCameraMovement:
            # Move the camera if the left mouse key is pressed and the mouse moved
            mouseMoveX = (self.mousePos.getX() - x) * (self.mouseSpeed + globalClock.getDt())
            mouseMoveY = (self.mousePos.getY() - y) * (self.mouseSpeed + globalClock.getDt())
            self.mousePos = Point2(x, y)

            if not self.movePivot:
                # Rotate the pivot point
                preP = self.pivot.getP()
                self.pivot.setP(0)
                self.pivot.setH(self.pivot, mouseMoveX)
                self.pivot.setP(preP)
                self.pivot.setP(self.pivot, mouseMoveY)
            else:
                # Move the pivot point
                self.pivot.setX(self.pivot, -mouseMoveX)
                self.pivot.setZ(self.pivot, mouseMoveY)

        # set the cameras zoom
        camera.setY(self.camDistance)

        # always look at the pivot point
        camera.lookAt(self.pivot)

        # continue the task until it got manually stopped
        return task.cont

runner = Runner()
runner.run()
