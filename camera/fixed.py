# this will show how to "set up" a fix positioned camera.
# To be clear, this will basically just show how to position
# a panda3d Camera and let it look at a specific position

from direct.showbase.ShowBase import ShowBase
base = ShowBase()

environ = loader.loadModel("environment")
environ.reparentTo(render)

# Important, disable panda3ds default mouse-camera control, otherwise the camera
# can not be set easily by the usual setPos, lookAt, setHpr etc... functions.
# Note: There is a way to position the cam even without caling disableMouse, but
#       that's not part of this snippet
base.disableMouse()
# position the camera at 10 units in x/y and z direction
camera.setPos(10, 10, 10)
# now let it look at the center
camera.lookAt(0,-10,5)

# you can also apply an interval to make the cam move
from direct.interval.IntervalGlobal import Sequence
left = camera.getH() - 35
right = camera.getH() + 35
# Rotate the camera from it's current position to the left in 2 seconds
camInterval1 = camera.hprInterval(3.0, (left, camera.getP(), camera.getR()))
# Rotate the camera to the right in 3 seconds
camInterval2 = camera.hprInterval(4.0, (right, camera.getP(), camera.getR()))
# move the camera back to it's original position so we don't get a jump when
# the interval starts again
camInterval3 = camera.hprInterval(3.0, (camera.getHpr()))
# setup and loop the camera movement sequence
camSequence = Sequence(camInterval1, camInterval2, camInterval3)
camSequence.loop()

base.run()
