from panda3d.core import Shader, LVecBase4, LVecBase3, PointLight, VBase4, Texture
from direct.showbase.ShowBase import ShowBase

base = ShowBase()

# laod a test model
panda = loader.loadModel("panda")
#panda = loader.loadModel("../../../data/models/fox/Fox")
panda.reparentTo(render)

plight = PointLight("plight")
plight.setColor(VBase4(1, 1, 1, 1))
plnp = render.attachNewNode(plight)
plnp.setPos(10, 20, 0)
render.setLight(plnp)

# set some information to be used in the shader
panda.set_shader_input("LightPosition", plnp.getPos(), 1)
panda.set_shader_input("LightColor", plight.getColor(), 1)

colorRampTex = Texture("ColorRamp")
colorRampTex.read("./1DTextureColorRamp.png")
colorRampTex.setWrapU(Texture.WM_clamp)
colorRampTex.setWrapV(Texture.WM_clamp)
#colorRampTex.setupTexture(Texture.TT_1d_texture, 256, 1, 1, Texture.T_int, Texture.F_rgba)
panda.set_shader_input("ColorRamp", colorRampTex)
render.setShaderAuto()
# load and apply the shader
panda.setShader(
    Shader.load(
        Shader.SLGLSL,
        "./half-lambert-vert.glsl",
        "./half-lambert-frag.glsl"))

# start the application
base.run()
