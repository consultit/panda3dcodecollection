// fragment shader for simple Phongs Lighting model
#version 430
#pragma optimize(off)
#pragma debug(on)
uniform sampler2D p3d_Texture0;
// Light position, in camera coordinates
uniform vec3 LightPosition;
// The color of our light
uniform vec4 LightColor;
// The color ramp used for the shading
//uniform sampler1D ColorRamp;
uniform sampler2D ColorRamp;
smooth in vec4 p3d_ColorInterp;
smooth in vec2 TextCoordInterp;
smooth in vec3 NormalInterp;
smooth in vec3 PositionInterp;
out vec4 FBColor;

vec4 getColorWrap(float coord)
{
    return vec4(texture(ColorRamp, vec2(coord, 0.0)).rgb, 1.0);
}

void main()
{
    /* After interpolation, normals probably are denormalized,
    so we need renormalize them */
    vec3 normal = normalize(NormalInterp);
    /*Calculate the rest of parameters exactly like we did in the
    vertex shader version of this shader*/
    vec3 lightVec = normalize(LightPosition - PositionInterp);
    vec3 viewVec = normalize(-PositionInterp);
    vec3 reflectVec = reflect(-lightVec, normal);
    float spec = max(dot(reflectVec, viewVec), 0.0);
    spec = pow(spec, 16.0);
    vec4 textureColor = texture(p3d_Texture0, TextCoordInterp);
    vec4 specContrib = LightColor * spec;
    // No ambient contribution this time
    vec4 ambientContrib = vec4(0.0, 0.0, 0.0, 0.0);
    //vec4 diffContrib = p3d_ColorInterp * LightColor * pow((0.5 * max(dot(lightVec, normal), 0.0) + 0.5), 2.0);
    vec4 diffContrib = p3d_ColorInterp * LightColor * getColorWrap(pow((0.5 * max(dot(lightVec, normal), 0.0) + 0.5), 2.0));
    /* Apply the mask to the specular contribution. The "1.0 -" is
    To invert the texture's alpha channel */
    vec4 lightContribution = ambientContrib + diffContrib + (specContrib * (1.0 - textureColor.a));
    FBColor = textureColor * lightContribution;
    //FBColor = getColorWrap(pow((0.5 * max(dot(lightVec, normal), 0.0) + 0.5), 2.0));
}
