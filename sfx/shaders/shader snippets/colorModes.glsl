//GLSL

/*
  This file contains function for various collor combining modes.

  Currently it contains:
    Add
    Mix
    Divide
    ...
*/


vec4 add(vec4 color_a, vec4 color_b)
{
    return min(color_a + color_b, 1.0);
}

vec4 subtract(vec4 color_a, vec4 color_b)
{
    return max(color_a - color_b, 0.0);
}

vec4 multiply(vec4 color_a, vec4 color_b)
{
    return color_a * color_b;
}

vec4 divide(vec4 color_a, vec4 color_b)
{
    return color_a / color_b;
}

vec4 mix(vec4 color_a, vec4 color_b)
{
    // ... this can't be correct..
    return color_a.a * color_b + (1 - color_a.a) * color_a;
}

vec4 screen(vec4 color_a, vec4 color_b)
{
    return 1 - (1 - color_a) * (1 - color_b);
}

vec4 overlay(vec4 color_a, vec4 color_b)
{
    // check if this is correct
    if(color_a <= 0.5) {
        return (2 * color_a) * color_b;
    } else {
        return 1 - (1 - 2 * (color_a - 0.5)) * (1 - color_b);
    }
}

vec4 difference(vec4 color_a, vec4 color_b)
{
    return abs(color_a - color_b);
}

vec4 darken(vec4 color_a, vec4 color_b)
{
    return min(color_a, color_b);
}

vec4 lighten(vec4 color_a, vec4 color_b)
{
    return max(color_a, color_b);
}

vec4 dodge(vec4 color_a, vec4 color_b)
{
    return color_a / (1.0 - color_b);
}

vec4 burn(vec4 color_a, vec4 color_b)
{
    return 1 - (1 - color_a) / color_b;
}

vec4 color(vec4 color_a)
{
    // check if this is correct
    vec4 tint = mix(color_a, vec4(1,1,1,1));
    return color_a + tint;
}

vec4 value(vec4 color_a, vec4 color_b)
{
    return 0;
}

vec4 saturation(vec4 color_a, vec4 color_b)
{
    return 0;
}

vec4 hue(vec4 color_a, vec4 color_b)
{
    return 0;
}
