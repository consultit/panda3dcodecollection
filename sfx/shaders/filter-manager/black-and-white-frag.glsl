// fragment shader
#version 430
#pragma debug(on)
#pragma optimize(off)
// Input from vertex shader
in vec2 texcoord;

uniform sampler2D p3d_Texture0;

out vec4 frameBufferColor;

void main()
{
    vec4 c = texture(p3d_Texture0, texcoord);

    // To have a useless filter that outputs the original view
    // without changing anything, just use :
    //o_color  = c;

    // basic black and white effet
    float moyenne = (c.x + c.y + c.z)/3;
    frameBufferColor = vec4(moyenne, moyenne, moyenne, 1);
}
