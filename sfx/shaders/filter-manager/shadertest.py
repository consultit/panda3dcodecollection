# This is a GLSL rewrite of the "The Simplest Filter" from the Panda3D
# Manual.

from panda3d.core import Shader, Vec4, Vec3, Vec2, Texture
from direct.filter.FilterManager import FilterManager
from direct.showbase.ShowBase import ShowBase

base = ShowBase()

# laod a test model
panda = loader.loadModel("panda")
panda.reparentTo(render)


manager = FilterManager(base.win, base.cam)
tex = Texture()
quad = manager.renderSceneInto(colortex=tex)
# load and apply the shader
quad.setShader(
    Shader.load(
        Shader.SLGLSL,
                "./black-and-white-vert.glsl",
                "./black-and-white-frag.glsl"))
# set some information to be used in the shader

# calculate texture padding
# NOTE: This doesn't have to be done if the config variable
# textures-power-2 is set to None
sx = tex.get_x_size() - tex.get_pad_x_size();
sy = tex.get_y_size() - tex.get_pad_y_size();
sz = tex.get_z_size() - tex.get_pad_z_size();
cx = (sx * 0.5) / tex.get_x_size();
cy = (sy * 0.5) / tex.get_y_size();
cz = (sz * 0.5) / tex.get_z_size();

quad.set_shader_input("tex_padding", Vec3(cx, cy, cz))

# start the application
base.run()
