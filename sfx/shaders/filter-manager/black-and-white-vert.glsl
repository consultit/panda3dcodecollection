// Vertex shader
#version 430
#pragma debug(on)
#pragma optimize(off)
in vec4 p3d_Vertex;

in vec2 p3d_MultiTexCoord0;
uniform vec3 tex_padding;
uniform mat4 p3d_ModelViewProjectionMatrix;

// Output to fragment shader
out vec2 texcoord;

void main()
{
    gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
    texcoord = p3d_Vertex.xz * tex_padding.xy + tex_padding.xy;
}
