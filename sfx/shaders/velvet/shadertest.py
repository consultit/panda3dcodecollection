from panda3d.core import Shader, LVecBase4, LVecBase3, PointLight, VBase4, Texture, Vec3, Vec4, AntialiasAttrib, Mat4
from direct.showbase.ShowBase import ShowBase

base = ShowBase()

# laod a test model
panda = loader.loadModel("panda")
panda.setScale(0.5)
panda.reparentTo(render)

lightDum=render.attachNewNode("lightDummy")
light=loader.loadModel("misc/sphere")
light.reparentTo(lightDum)
light.setPos(0,15,5)
lightDum.setShaderOff(1)
lightDum.hprInterval(5,Vec3(360,0,0)).loop()

# default values
facingRatioPower = 0.7
envirLightColor = Vec4(1,1,1,0)

# set some information to be used in the shader
render.setShaderInput("cam", camera)
render.setShaderInput("light", light)
render.setShaderInput("envirLightColor", envirLightColor * facingRatioPower)

#render.setAntialias(AntialiasAttrib.MMultisample)

# load and apply the shader
render.setShader(
    Shader.load(
        Shader.SLGLSL,
        "./velvet-vert.glsl",
        "./velvet-frag.glsl"))

camera.setPos(-15, -15, 8)
camera.lookAt(0, 0, 2.5)
mat=Mat4(camera.getMat())
mat.invertInPlace()
base.mouseInterfaceNode.setMat(mat)
base.setBackgroundColor(0.2,0.2,0.2,1)

def updateFacingRatioPower(value):
    global facingRatioPower
    facingRatioPower += value
    render.setShaderInput("envirLightColor", envirLightColor * facingRatioPower)

base.accept("+", updateFacingRatioPower, [0.05])
base.accept("-", updateFacingRatioPower, [-0.05])

# start the application
base.run()
