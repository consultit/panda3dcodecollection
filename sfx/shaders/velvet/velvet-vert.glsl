// Vertex part for velvet shader
#version 430
#pragma optimize(off)
#pragma debug(on)
in vec4 p3d_Vertex;
in vec4 p3d_Color;
in vec2 p3d_MultiTexCoord0;
in vec3 p3d_Normal;
uniform mat4 p3d_ModelViewProjectionMatrix;
uniform vec3 mspos_cam;
uniform vec3 mspos_light;

smooth out vec2 l_texcoord0;
smooth out float l_smooth;
smooth out float l_facingRatio;
smooth out vec4 vertexColor;

void main()
{
    // SMOOTH SHADING : dot product ranges from -1~1, scale it so I get 0~1,
    // but before it, I set the ambient light to .2,
    // so it's .5/2.5 (2.5 is -1.5~1 range), add the .5 to the dark side
    l_smooth = smoothstep(-1.5, 1.0, dot(p3d_Normal, normalize(mspos_light - p3d_Vertex.xyz)));
    l_facingRatio = pow(1.0 - clamp(dot(p3d_Normal, normalize(mspos_cam - p3d_Vertex.xyz)), 0.0, 1.0), 2);

    // forward the vertex color and texture coordinates to the fragmentshader
    l_texcoord0 = p3d_MultiTexCoord0;
    vertexColor = p3d_Color;

    gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
}

/*
// Original code by ynjh_jo
void vshader(
             float4 vtx_position : POSITION,
             float2 vtx_texcoord0 : TEXCOORD0,
             float3 vtx_normal : NORMAL,
             uniform float4x4 mat_modelproj,
             uniform float4 mspos_cam,
             uniform float4 mspos_light,

          out float  l_smooth,
          out float  l_facingRatio,
             out float2 l_texcoord0 : TEXCOORD0,
             out float4 l_position : POSITION
             )
{
  // SMOOTH SHADING : dot product ranges from -1~1, scale it so I get 0~1,
  // but before it, I set the ambient light to .2,
  // so it's .5/2.5 (2.5 is -1.5~1 range), add the .5 to the dark side
  l_smooth = smoothstep( -1.5,1,dot(vtx_normal, normalize(mspos_light-vtx_position)) );
  l_facingRatio = pow( 1.0-saturate( dot(vtx_normal, normalize(mspos_cam-vtx_position)) ), 2 );
  l_position = mul(mat_modelproj, vtx_position);
  l_texcoord0=vtx_texcoord0;
}
*/
