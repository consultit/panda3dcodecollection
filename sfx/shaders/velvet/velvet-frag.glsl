// fragment  part for velvet shader
#version 430
#pragma optimize(off)
#pragma debug(on)
uniform sampler2D p3d_Texture0;
smooth in vec2 l_texcoord0;
smooth in float l_smooth;
smooth in float l_facingRatio;
smooth in vec4 vertexColor;
uniform vec4 envirLightColor;
out vec4 FBColor;

void main()
{
    vec4 tex = texture(p3d_Texture0, l_texcoord0);

    // Calculate the objects base color using texture and vertex color
    vec4 obj_color = tex * vertexColor;

    // Blend from object color to environment color using the facing ratio
    FBColor = vec4(mix(obj_color.rgb * l_smooth, (obj_color.rgb + envirLightColor.rgb) * 0.5, l_facingRatio), obj_color.a);
}

/*
// Original code by ynjh_jo
void fshader(
    in float2 l_texcoord0: TEXCOORD0,
    in float  l_smooth,
    in float  l_facingRatio,
    uniform float4 k_envirLightColor,
    sampler2D tex_0 : TEXUNIT0,

    out float4 o_color:COLOR)
{
    float4 tex = tex2D(tex_0, l_texcoord0);
    // o_color = float4( tex.rgb*l_smooth, tex.a );
    // o_color = float4( lerp(tex.rgb*l_smooth, k_envirLightColor.rgb, l_facingRatio) , tex.a );
    o_color = float4( lerp(tex.rgb*l_smooth, (tex.rgb+k_envirLightColor.rgb)*.5, l_facingRatio) , tex.a );
}
*/
