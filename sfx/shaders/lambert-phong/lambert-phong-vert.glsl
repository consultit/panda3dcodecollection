// Vertex shader for simple Phongs Lighting model
#version 430
#pragma optimize(off)
#pragma debug(on)
uniform mat4 p3d_ModelViewProjectionMatrix;
uniform mat4 p3d_ModelViewMatrix;
uniform mat4 p3d_ProjectionMatrix;
/* Matrix to transform normals. This is the transpose of the
inverse of the upper leftmost 3x3 of the modelview matrix */
uniform mat3 p3d_NormalMatrix;
in vec4 p3d_Vertex;
in vec2 p3d_MultiTexCoord0;
in vec3 p3d_Normal;
in vec4 p3d_Color;
smooth out vec2 TextCoordInterp;
smooth out vec3 PositionInterp;
smooth out vec4 p3d_ColorInterp;
/*This time we will interpolate the Normal, not the total computed
lighting contribution*/
smooth out vec3 NormalInterp;

void main()
{
    PositionInterp = vec4(p3d_ModelViewMatrix * p3d_Vertex).xyz;
    TextCoordInterp = p3d_MultiTexCoord0;
    // Normals transform
    NormalInterp = normalize(p3d_NormalMatrix * p3d_Normal);
    gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
    p3d_ColorInterp = p3d_Color;
}
