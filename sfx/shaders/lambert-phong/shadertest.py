from panda3d.core import Shader, LVecBase4, LVecBase3, PointLight, VBase4
from direct.showbase.ShowBase import ShowBase

base = ShowBase()

# laod a test model
panda = loader.loadModel("panda")
panda.reparentTo(render)

plight = PointLight('plight')
plight.setColor(VBase4(1, 1, 1, 1))
plnp = render.attachNewNode(plight)
plnp.setPos(10, 20, 0)
render.setLight(plnp)

# set some information to be used in the shader
panda.set_shader_input("LightPosition", plnp.getPos(), 1)
panda.set_shader_input("LightColor", plight.getColor(), 1)

# load and apply the shader
panda.setShader(
    Shader.load(
        Shader.SLGLSL,
        "./lambert-phong-vert.glsl",
        "./lambert-phong-frag.glsl"))

# start the application
base.run()
