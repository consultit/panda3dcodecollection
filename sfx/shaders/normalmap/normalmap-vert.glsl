#version 430
#pragma optimize(off)
#pragma debug(on)
in vec4 p3d_Vertex;
in vec3 p3d_Normal;
in vec2 p3d_MultiTexCoord0;

uniform mat4 p3d_ModelViewProjectionMatrix;
uniform mat4 p3d_ModelViewMatrix;
uniform mat3 p3d_NormalMatrix;
in vec3 lightpos;

in vec3 p3d_Tangent;
smooth out vec3 lightVec;
smooth out vec3 halfVec;
smooth out vec2 l_texcoord0;


void main()
{
    // Building the matrix Eye Space -> Tangent Space
    vec3 n = normalize(p3d_NormalMatrix * p3d_Normal);
    vec3 t = normalize(p3d_NormalMatrix * p3d_Tangent);
    vec3 b = cross(n, t);

    vec3 vertexPosition = vec4(p3d_ModelViewMatrix *  p3d_Vertex).xyz;
    vec3 lightDir = normalize(lightpos - vertexPosition);


    // transform light and half angle vectors by tangent basis
    vec3 v;
    v.x = dot(lightDir, t);
    v.y = dot(lightDir, b);
    v.z = dot(lightDir, n);
    lightVec = normalize (v);

    vertexPosition = normalize(vertexPosition);

    // Normalize the halfVector to pass it to the fragment shader
    vec3 halfVector = normalize(vertexPosition + lightDir);
    v.x = dot(halfVector, t);
    v.y = dot(halfVector, b);
    v.z = dot(halfVector, n);

    // No need to normalize, t,b,n and halfVector are normal vectors.
    //normalize (v);
    halfVec = v;

    l_texcoord0 = p3d_MultiTexCoord0;

    gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
}
