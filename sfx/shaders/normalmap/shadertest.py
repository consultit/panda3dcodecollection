from panda3d.core import Shader, PointLight, AmbientLight, Vec4
from direct.showbase.ShowBase import ShowBase

base = ShowBase()

# laod a test model
model = loader.loadModel("../../../data/models/fox/Fox")
model.reparentTo(render)

alight = AmbientLight("alight")
alight.setColor(Vec4(0.05, 0.05, 0.05, 1))
alnp = render.attachNewNode(alight)
render.setLight(alnp)

plight = PointLight("plight")
plight.setColor(Vec4(1, 1, 1, 1))
plnp = render.attachNewNode(plight)
plnp.setPos(10, 20, 0)
render.setLight(plnp)

# set some information to be used in the shader
model.set_shader_input("lightcolor", plight.getColor())
model.set_shader_input("lightpos", plnp)

# load and apply the shader
model.setShader(
    Shader.load(
        Shader.SLGLSL,
        "./normalmap-vert.glsl",
        "./normalmap-frag.glsl"))

# start the application
base.run()
