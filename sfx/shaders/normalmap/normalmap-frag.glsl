#version 430
#pragma optimize(off)
#pragma debug(on)
uniform sampler2D p3d_Texture0;
uniform sampler2D p3d_Texture1;
uniform vec4 lightcolor;
uniform struct {
  vec4 ambient;
} p3d_LightModel;

smooth in vec2 l_texcoord0;
// New bumpmapping
smooth in vec3 lightVec;
smooth in vec3 halfVec;

out vec4 FBColor;


void main()
{
    // lookup normal from normal map, move from [0,1] to  [-1, 1] range, normalize
    vec3 normal = 2.0 * texture(p3d_Texture1, l_texcoord0).rgb - 1.0;
    normal = normalize(normal);

    // compute diffuse lighting
    float lamberFactor= max(dot(lightVec, normal), 0.0);
    vec4 diffuseMaterial = texture(p3d_Texture0, l_texcoord0);
    vec4 diffuseLight = lightcolor;

    // compute specular lighting
    vec4 specularMaterial;
    vec4 specularLight;
    float shininess;

    // compute ambient
    vec4 ambientLight = vec4(p3d_LightModel.ambient.rgb, 1.0);

    if(lamberFactor > 0.0)
    {
        // In doom3, specular value comes from a texture
        // specularMaterial should be set from a spec map texture if available
        specularMaterial = vec4(1,1,1,1);
        specularLight = lightcolor;
        shininess = pow(max(dot(halfVec, normal), 0.0), 2.0);

        FBColor = diffuseMaterial * diffuseLight * lamberFactor;
        FBColor += specularMaterial * specularLight * shininess;
        FBColor += ambientLight;
    } else {
        FBColor = diffuseMaterial * ambientLight;
    }

}
