// fragment shader
#version 430
#pragma debug(on)
#pragma optimize(off)
// Interpolator for our light
smooth in vec4 lightContribution;
out vec4 frameBufferColor;

uniform sampler2D p3d_Texture0;

// Input from vertex shader
in vec2 texcoord;

void main()
{
    vec4 tex = texture(p3d_Texture0, texcoord);
    frameBufferColor = lightContribution * tex.rgba;
}
