from panda3d.core import Shader, Vec4, Vec3
from direct.showbase.ShowBase import ShowBase

base = ShowBase()

# laod a test model
panda = loader.loadModel("panda")
panda.reparentTo(render)

# set some information to be used in the shader
panda.set_shader_input("LightPosition", Vec3(-5, -5, 7), 1)
panda.set_shader_input("LightColor", Vec4(1, 1, 1, 1), 1)

# load and apply the shader
panda.setShader(
    Shader.load(
        Shader.SLGLSL,
        "./per-vertex-vert.glsl",
        "./per-vertex-frag.glsl"))

# start the application
base.run()
