// Vertex shader
#version 430
#pragma debug(on)
#pragma optimize(off)
in vec4 p3d_Vertex;
// Vertex normals buffer mapping
in vec3 p3d_Normal;
in vec4 p3d_Color;

// Texture coordinates
in vec2 p3d_MultiTexCoord0;

// Interpolator for our light
smooth out vec4 lightContribution;

// Output to fragment shader
out vec2 texcoord;

/* Matrix to transform normals. This is the transpose of the
inverse of the upper leftmost 3x3 of the modelview matrix */
uniform mat3 p3d_NormalMatrix;
uniform mat4 p3d_ModelViewProjectionMatrix;
uniform mat4 p3d_ModelViewMatrix;

// We need to know where the light emitter is located
uniform vec3 LightPosition;

// The color of our light
uniform vec4 LightColor;

// Note: All vectors must always be normalized
void main()
{
    gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
    vec3 normal = normalize(p3d_NormalMatrix * p3d_Normal);

    // Compute vertex position in camera coordinates
    vec4 worldVertexPos = p3d_ModelViewMatrix * p3d_Vertex;

    // Compute the vector that comes from the light
    vec3 lightVec = normalize(LightPosition - worldVertexPos.xyz);

    /* Compute the vector that comes from the camera. Because we
    are working in camera coordinates, camera is at origin so the
    calculation would be:
    normalize(vec3(0,0,0) - worldVertexPos.xyz); */
    vec3 viewVec = normalize(-worldVertexPos.xyz);

    /* Calculate the specular contribution. Reflect is a built-in
    function that returns the vector that is the reflection of
    a vector on a surface represented by its normal */
    vec3 reflectVec = reflect(-lightVec, normal);
    float spec = max(dot(reflectVec, viewVec), 0.0);
    spec = pow(spec, 16.0);
    vec4 specContrib = LightColor * spec;

    /* We don't want any ambient contribution, but
    let's write it down for teaching purposes */
    vec4 ambientContrib = vec4(0,0,0,0);

    // Calculate diffuse contribution
    vec4 diffContrib = LightColor * p3d_Color.rgba * max(dot(lightVec, normal), 0);

    /* Final light contribution that will be interpolated in the
    fragment shader */
    lightContribution = ambientContrib + diffContrib + specContrib;

    texcoord = p3d_MultiTexCoord0;
}
