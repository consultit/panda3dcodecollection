from panda3d.core import Shader, LVecBase4, LVecBase3
from direct.showbase.ShowBase import ShowBase

base = ShowBase()

# laod a test model
panda = loader.loadModel("ShadertestCube")
panda.reparentTo(render)

# load and apply the shader
panda.setShader(
    Shader.load(
        Shader.SLGLSL,
        "./texscale-vert.glsl",
        "./texscale-frag.glsl"))

# start the application
base.run()
