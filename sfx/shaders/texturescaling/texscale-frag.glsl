// fragment shader
#version 430
#pragma debug(on)
#pragma optimize(off)
// Interpolator for our light
smooth in vec4 lightContribution;
out vec4 frameBufferColor;

uniform sampler2D p3d_Texture0; // First texture layer (the purple lines)
uniform sampler2D p3d_Texture1; // Second texture layer (the black cross)

// Input from vertex shader
in vec2 texcoord0;
in vec2 texcoord1;

void main()
{
    vec4 color_a = texture(p3d_Texture0, texcoord0);
    vec4 color_b = texture(p3d_Texture0, texcoord1);
    // multiply colors
    frameBufferColor = color_a;
}
