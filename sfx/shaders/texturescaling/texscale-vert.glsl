// Vertex shader
#version 430
#pragma debug(on)
#pragma optimize(off)
in vec4 p3d_Vertex;

// Texture manipulation matrix
uniform mat4 p3d_TextureMatrix[];

// Texture coordinates
in vec2 p3d_MultiTexCoord0; // coordinates for the purple lines
in vec2 p3d_MultiTexCoord1; // coordinates for the black cross

// Output to fragment shader
out vec2 texcoord0;
out vec2 texcoord1;

/* Matrix to transform normals. This is the transpose of the
inverse of the upper leftmost 3x3 of the modelview matrix */
uniform mat4 p3d_ModelViewProjectionMatrix;


// Most simple shader for texture usage
void main()
{
    gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;

    // color A
    texcoord0 = vec2(vec4(p3d_MultiTexCoord0, 0, 1) * p3d_TextureMatrix[1]);

    // color B
    texcoord1 = vec2(vec4(p3d_MultiTexCoord1, 0, 1) * p3d_TextureMatrix[0]);
}
