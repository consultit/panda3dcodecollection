import sys

from direct.showbase.ShowBase import ShowBase
#from direct.showbase.MirrorDemo import setupMirror
from direct.showbase.OnScreenDebug import OnScreenDebug
from panda3d.core import Vec3, Vec4, LVecBase3f, LPlanef, VBase4, LMatrix4f
from panda3d.core import Camera, PerspectiveLens, Lens
from panda3d.core import Plane, Point3, CullFaceAttrib, CardMaker
from panda3d.core import TextureStage
from panda3d.core import NodePath, PandaNode, PlaneNode

class Runner(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        # accept the esc button to close the application
        self.accept("escape", sys.exit)

        self.osd = OnScreenDebug()
        # enable the on-screen-debug
        self.osd.add("cam.mirror", "")
        self.osd.add("cam.main", "")
        self.osd.enabled = True
        # render the osd so it really gets displayed
        # use this function to update the osd
        self.osd.render()

        # simple level
        self.environ = loader.loadModel("environment")
        self.environ.reparentTo(self.render)

        self.setupFPCam()

        #self.setupMirrorCam()
        mirror = self.setupMirror("Mirror", 500, 500, self.camera)
        mirror.setZ(0.5)
        mirror.setP(90)

        #base.disableMouse()
        #base.camera.setPos(100, 100, 200)
        #base.camera.lookAt(0,0,0)


    def setupMirrorCam(self):
        self.reflectionBuffer=base.win.makeTextureBuffer("waterreflectionbuffer", 2048*2, 2048*2)
        self.reflectionBuffer.setClearColor( Vec4( 0, 0, 0, 1 ) )
        self.reflectionCam=base.makeCamera(self.reflectionBuffer)
        self.reflectionCam.setH(180)

        self.mirrorPlane = Plane(Point3(-1, 1, 0.5), Point3(-1, -1, 0.5), Point3(1, -1, 0.5))#Vec3(0, 0, 1), Point3(0, 0, 1))
        self.PlaneNP = NodePath(PlaneNode("mirrorPlane", self.mirrorPlane))
        #np = NodePath('np')
        #np.setAttrib(CullFaceAttrib.makeReverse())
        #np.setClipPlane(self.PlaneNP)
        render.setClipPlane(self.PlaneNP)
        #self.reflectionCam.node().setInitialState(np.getState())

        mirrorCM = CardMaker("mirrorCM")
        mirrorCM.setFrame(-500, 500, -500, 500)
        self.mirrorNP = render.attachNewNode(mirrorCM.generate())
        self.mirrorNP.setPos(0, 0, 0.6)
        self.mirrorNP.setP(-90)
        tex0 = self.reflectionBuffer.getTexture()
        ts0 = TextureStage("reflection")
        self.mirrorNP.setTexture(ts0, tex0)



    def setupMirror(self, name, width, height, rootCamera = None, parent=None):
        # The return value is a NodePath that contains a rectangle that
        # reflects render.  You can reparent, reposition, and rotate it
        # anywhere you like.
        if rootCamera is None:
            rootCamera = base.camera

        if parent is None:
            parent = render
        root = parent.attachNewNode(name)

        # Create a polygon to be the visible representation of the mirror.
        cm = CardMaker('mirror')
        cm.setFrame(width / 2.0, -width / 2.0, -height / 2.0, height / 2.0)
        cm.setHasUvs(1)
        card = root.attachNewNode(cm.generate())

        # Create a PlaneNode to represent the mirror's position, for
        # computing where the mirror's camera belongs each frame.
        plane = Plane(Vec3(0, 1, 0), Point3(0, 0, 0))
        planeNode = PlaneNode('mirrorPlane')
        planeNode.setPlane(plane)
        planeNP = root.attachNewNode(planeNode)

        # Now create an offscreen buffer for rendering the mirror's point
        # of view.  The parameters here control the resolution of the
        # texture.
        buffer = base.win.makeTextureBuffer(name, 256, 256)
        #buffer.setClearColor(base.win.getClearColor())
        buffer.setClearColor(VBase4(0, 0, 1, 1))

        # Set up a display region on this buffer, and create a camera.
        dr = buffer.makeDisplayRegion()
        camera = Camera('mirrorCamera')
        lens = PerspectiveLens()
        lens.setFilmSize(width, height)
        camera.setLens(lens)
        cameraNP = planeNP.attachNewNode(camera)
        dr.setCamera(cameraNP)

        # Since the reflection matrix will reverse the vertex-winding
        # order of all the polygons in the world, we have to tell the
        # camera to reverse the direction of its face culling.  We also
        # tell it not to draw (that is, to clip) anything behind the
        # mirror plane.
        dummy = NodePath('dummy')
        dummy.setAttrib(CullFaceAttrib.makeReverse())
        dummy.setClipPlane(planeNP)
        camera.setInitialState(dummy.getState())

        # Create a visible representation of the camera so we can see it.
        #cameraVis = loader.loadModel('camera.egg')
        #if not cameraVis.isEmpty():
        #    cameraVis.reparentTo(cameraNP)

        # Spawn a task to keep that camera on the opposite side of the
        # mirror.
        def moveCamera(task, cameraNP = cameraNP, plane = plane,
                       planeNP = planeNP, card = card, lens = lens,
                       width = width, height = height, rootCamera = rootCamera):
            # Set the camera to the mirror-image position of the main camera.
            cameraNP.setMat(rootCamera.getMat(planeNP) * plane.getReflectionMat())

            # And reset the frustum to exactly frame the mirror's corners.
            # This is a minor detail, but it helps to provide a realistic
            # reflection and keep the subject centered.
            ul = cameraNP.getRelativePoint(card, Point3(-width / 2.0, 0, height / 2.0))
            ur = cameraNP.getRelativePoint(card, Point3(width / 2.0, 0, height / 2.0))
            ll = cameraNP.getRelativePoint(card, Point3(-width / 2.0, 0, -height / 2.0))
            lr = cameraNP.getRelativePoint(card, Point3(width / 2.0, 0, -height / 2.0))
            lens.setFrustumFromCorners(ul, ur, ll, lr, Lens.FCCameraPlane | Lens.FCOffAxis | Lens.FCAspectRatio)

            cameraNP.setMat(cameraNP.getMat())# * LMatrix4f(1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1))

            return task.cont

        # Add it with a fairly high priority to make it happen late in the
        # frame, after the avatar controls (or whatever) have been applied
        # but before we render.
        taskMgr.add(moveCamera, name, priority = 40)

        # Now apply the output of this camera as a texture on the mirror's
        # visible representation.
        card.setTexture(buffer.getTexture())

        return root

    def setupFPCam(self):
        # enable movements through the level
        self.keyMap = {"left":0, "right":0, "forward":0, "backward":0, "up":0, "down":0}
        self.player = self.loader.loadModel("smiley")
        self.player.setPos(0, 0, 0)
        self.player.reparentTo(self.render)
        self.accept("w", self.setKey, ["forward",1])
        self.accept("w-up", self.setKey, ["forward",0])
        self.accept("a", self.setKey, ["left",1])
        self.accept("a-up", self.setKey, ["left",0])
        self.accept("s", self.setKey, ["backward",1])
        self.accept("s-up", self.setKey, ["backward",0])
        self.accept("d", self.setKey, ["right",1])
        self.accept("d-up", self.setKey, ["right",0])
        self.accept("r", self.setKey, ["up",1])
        self.accept("r-up", self.setKey, ["up",0])
        self.accept("f", self.setKey, ["down",1])
        self.accept("f-up", self.setKey, ["down",0])

        # disable pandas default mouse-camera controls so we can handle the cam
        # movements by ourself
        self.disableMouse()

        # screen sizes
        self.winXhalf = base.win.getXSize() // 2
        self.winYhalf = base.win.getYSize() // 2

        self.mouseSpeedX = 0.1
        self.mouseSpeedY = 0.1

        self.camViewTarget = LVecBase3f()

        self.camera.setH(180)
        self.camera.reparentTo(self.player)
        self.camera.setZ(self.player, 2)

        # maximum and minimum pitch
        self.maxPitch = 90
        self.minPitch = -80

        # the players movement speed
        self.movementSpeedForward = 25
        self.movementSpeedBackward = 25
        self.striveSpeed = 25

        self.taskMgr.add(self.move, "moveTask", priority=-4)

    def setKey(self, key, value):
        self.keyMap[key] = value

    def move(self, task):
        # Reflections matrix calculations
        #self.reflectionCam.setPosHpr(base.camera.getPos(), base.camera.getHpr())
        #self.reflectionCam.setHpr(base.camera.getHpr())
        #print(self.reflectionCam.getParent())
        # mc = base.camera.getMat(self.reflectionCam.getParent())
        #mc = self.reflectionCam.getMat()
        # mf = self.mirrorPlane.getReflectionMat()
        # self.reflectionCam.setMat(mc * mf)

        mirrorCam = render.find("**/mirrorCamera")

        self.osd.add("cam.mirror", "Pos {}\n\t\tHPR {}".format(mirrorCam.getPos(render), mirrorCam.getHpr(render)))
        self.osd.add("cam.main", "Pos {}\n\t\tHPR {}".format(camera.getPos(render), camera.getHpr(render)))
        self.osd.render()


        if not base.mouseWatcherNode.hasMouse(): return task.cont

        pointer = base.win.getPointer(0)
        mouseX = pointer.getX()
        mouseY = pointer.getY()

        if base.win.movePointer(0, self.winXhalf, self.winYhalf):
            # calculate the looking up/down of the camera.
            # NOTE: for first person shooter, the camera here can be replaced
            # with a controlable joint of the player model

            # calculate the pitch the camera should directly move to
            p = self.camera.getP() - (mouseY - self.winYhalf) * self.mouseSpeedY
            # some sanity checks
            if p < self.minPitch:
                p = self.minPitch
            elif p > self.maxPitch:
                p = self.maxPitch

            # directly set the cameras pitch
            self.camera.setP(p)
            # actualize the target so we don't get an unexpected
            # move if we toggle between cinematic and non cinematic
            # mode
            self.camViewTarget.setY(p)

            # rotate the player's heading according to the mouse x-axis movement
            # directly use the players heading to calculate the new
            # one
            h = self.player.getH() - (mouseX - self.winXhalf) * self.mouseSpeedX
            # some sanity checks
            if h < -360:
                h += 360
            elif h > 360:
                h -= 360
            self.player.setH(h)
            # actualize the target so we don't get an unexpected
            # move if we toggle between cinematic and non cinematic
            # mode
            self.camViewTarget.setX(h)

        # basic movement of the player
        if self.keyMap["left"] != 0:
            self.player.setX(self.player, self.striveSpeed * globalClock.getDt())
        if self.keyMap["right"] != 0:
            self.player.setX(self.player, -self.striveSpeed * globalClock.getDt())
        if self.keyMap["forward"] != 0:
            self.player.setY(self.player, -self.movementSpeedForward * globalClock.getDt())
        if self.keyMap["backward"] != 0:
            self.player.setY(self.player, self.movementSpeedBackward * globalClock.getDt())
        if self.keyMap["up"] != 0:
            self.player.setZ(self.player, self.movementSpeedForward * globalClock.getDt())
        if self.keyMap["down"] != 0:
            self.player.setZ(self.player, -self.movementSpeedForward * globalClock.getDt())

        return task.cont

runner = Runner()
runner.run()
