
from direct.showbase.ShowBase import ShowBase
from panda3d.core import PointLight, AmbientLight

base = ShowBase()

# Put two pandas in the scene, panda x and panda y.
x = loader.loadModel('panda')
x.reparentTo(render)
x.setPos(0,0,-6)

# Position the camera to view the two pandas.
base.trackball.node().setPos(0, 60, 0)

for i in range(15):
    pLight = PointLight("pLight")
    pLight.setColor((0.8, 0.8, 0.6, 1))
    pLight.setShadowCaster(True, 512, 512)
    pLightNP = render.attachNewNode(pLight)
    pLightNP.setPos(-7 + i, 0, 8)
    render.setLight(pLightNP)

# Create Ambient Light
ambientLight = AmbientLight('ambientLight')
ambientLight.setColor((0.2, 0.2, 0.2, 1))
ambientLightNP = render.attachNewNode(ambientLight)
render.setLight(ambientLightNP)

render.setShaderAuto()

base.run()
