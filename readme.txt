### Panda3D Code Collection ###

What it is:
This is a small collection of code snippets and recieps usefull for Panda3D game
development. It contains small tips and tricks which could be quickly
implemented in a game to make it better and more feature rich. It also has tips
on how to set up the core mechanics of games.

All these things inside the collection where collected over many years of
testing and gattering experience in game development. And over time, will grow
and change.


What it is NOT:
The collection is not a full game nor is it a game creation kit or and editor.
All the snippets have to be slightly changed to fit the needs of a full fledged
game.


How to use:
all samples and snippets are with a few exceptions single, executable python
files, so simply browse to the directory which they are in and use
python filename.py in a terminal to run them and see how they work. Most of them
can be closed using the Esc key. Note to see something in some of the samples,
you need to manually move the camera using the default P3D camera handling
    Right-click and mouse move to zoom in/out
    Left-click to move the pivot
    Middle-click to rotate around the pivot
If you find a snippet that can be of use for you, open the code file and check
the code, then copy the relevant parts over to your project or copy and edit
the whole snippet file.


Content description:

AI:
    The AI folder contains code snippets for Artifical Inteligence usage in
    games.

camera:
    Inside the camera folder, one can find various technics to control the
    camera inside a game. This can be first- and third-person cameras as well
    as side scrollers and top-down ones.

character:
    The character folder contains code that can be used to set up player 
    characters and NPCs. It mainly shows the usage of pandas actor class.

core:
    Inside the core folder, one can find all mechanics which are deep inside the
    core of the game. It contains things like event handling loading models and
    saving and handling the game states.

data:
    Data contains the source of the test graphics of the collection. Here are
    all the models, textures and sound files which do not come directly packed
    with panda3d.

GUI:
    This folder contains all things related to gui creation and things that can
    be done with pandas integrated directGUI. It contains things like loading-
    screens simple menus and HUDs

input:
    Inside input are all the ways that can be used to control things in a game
    it shows mouse and keyboard support of panda3d as well as implementing
    gamepads via pygame.

Internationalisation:
    This folder contains code that will help you to translate your applications
    in other languages.

multimedia:
    The multimedia folder contains code to show how to load audio and video
    files with panda3d and how to play them inside games.

network:
    Network will show how to set up panda3d games to be played over a lan
    connection.

physics:
    In this section are code snippets that show the usage of the physics engines
    of panda3d.

sfx:
    The sfx folder contains code for how to setup lights, particles and other
    more advanced things that add to the look and feel of games.

specials:
    The specials folder is filled with small advanced codes that will enhance
    a game, but are not really essencially for one. Like taking screenshots
    directly within panda windows or setting window titles as well as fullscreen
    toggle.
