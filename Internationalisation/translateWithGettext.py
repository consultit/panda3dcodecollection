import gettext
import os

path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "lng")

# the name "test" comes from the mo filename without the mo file extension
lang1 = gettext.translation("test", localedir=path, languages=['en_GB'], fallback=False)
lang2 = gettext.translation("test", localedir=path, languages=['fr_FR'], fallback=False)
lang3 = gettext.translation("test", localedir=path, languages=['de_DE'], fallback=False)



# start by using language1
lang1.install()
print (_("welcome"))

# ... time goes by, user selects language 2
lang2.install()
print (_("welcome"))

# ... more time goes by, user selects language 3
lang3.install()
print (_("welcome"))


#
# Global localization using system language settings
#
import builtins as __builtin__
# set the textdomain. The domain name, in this case 'test', will be the part
# in front of the .mo file. localedir would be the default location for language
# files so we set it to our other location "lng" which will have the following
# directory format
#
# localdir/[countrycode]/LC_MESSAGES/Domainname.mo
#
# countrycode should be the four letter code like en_US or de_DE
# the country code will then be taken from environment variables like LANGUAGE
gettext.bindtextdomain("test", "lng")
gettext.textdomain("test")
__builtin__._ = gettext.gettext
print (__builtin__._("welcome"))
