# here we will show, how to use panda3ds multifile files.

from direct.showbase.ShowBase import ShowBase
base = ShowBase()
import os
from panda3d.core import VirtualFileSystem
from panda3d.core import Filename

baseDir = os.path.join("..", "data")
# get the pointer to the virtual file system
vfs = VirtualFileSystem.getGlobalPtr()
# mount the path to pandas used directory structure at "." in readonly mode
vfs.mount(Filename(os.path.join(baseDir, "models.mf")),
          ".",
          VirtualFileSystem.MFReadOnly)

# test load a model of the multifile
wolf = loader.loadModel("models/wolf.egg")
# put it on the left side
wolf.setPos(-15, 100, 0)
wolf.reparentTo(render)

vfs.unmount(Filename(os.path.join(baseDir, "models.mf")))

# now mounting a directory on the system
vfs.mount(
    Filename(os.path.join(baseDir, "models")),
    ".",
    VirtualFileSystem.MFReadOnly)
# load a model from the file system
systemWolf = loader.loadModel("wolf")
# put it on the right side
systemWolf.setPos(15, 100, 0)
systemWolf.reparentTo(render)


base.run()
