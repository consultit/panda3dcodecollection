# this will show how to use the panda3d task system
# NOTE: it is better to use many small tasks instead of one big task

import direct.directbase.DirectStart
import sys

def simpletask(task):
    if task.time < 2.0:
        print ("Task is running")
        return task.cont
    print ("Task is done")
    return task.done

def kill(task):
    print ("exit now")
    #sys.exit(0)
    return task.done

taskMgr.add(simpletask, "test_task")
taskMgr.doMethodLater(2.1, kill, "quit the app")

run()
