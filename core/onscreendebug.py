from direct.showbase.ShowBase import ShowBase
from direct.showbase.OnScreenDebug import OnScreenDebug

class Main(ShowBase):
	"""Main class of the application
	initialise the engine (ShowBase)"""

	def __init__(self):
		"""initialise the engine"""
		ShowBase.__init__(self)
		#self.setBackgroundColor(0,0,0)

		self.osd = OnScreenDebug()

		# add a key value pair
		self.osd.add("Hello", "world")
		# check if we have the Key Hello and add that to the osd
		self.osd.append("\nHave Key 'Hello': {}".format(
						self.osd.has("Hello")))

		# add a simple text
		self.osd.append("HELLO WORLD:")
		# removes the HELLO WORLD text and everything else that has been
		# added by the append function
		self.osd.clear()
		# add some more values
		self.osd.append("Test 1")

		# add some new key value pairs
		self.osd.add("1-test1", "123")
		self.osd.add("1-test2", "123")
		self.osd.add("2-test3", "123")
		# remove all key value pairs that have been added with the add
		# method and have the given prefix in the key
		self.osd.removeAllWithPrefix("1-")


		self.osd.append("\nUse F1 to toggle the OSD")
		self.accept("f1", self.toggleOSD)

		# enable the on-screen-debug
		self.osd.enabled = True
		# render the osd so it really gets displayed
		# use this function to update the osd
		self.osd.render()

		print ("FRAME", self.osd.frame)
		print ("TEXT", self.osd.text)
		print ("DATA", self.osd.data)
		print ("ENABLED", self.osd.enabled)

	def toggleOSD(self):
		self.osd.enabled = not self.osd.enabled
		if self.osd.onScreenText:
			if self.osd.enabled:
				self.osd.onScreenText.show()
			else:
				self.osd.onScreenText.hide()

# instantiate the engine
base = Main()

# starting the application
if __name__ == '__main__':
	# start the application
	base.run()
