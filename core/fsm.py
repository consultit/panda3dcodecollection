"""basic fsm that moves through its states"""

from direct.showbase.ShowBase import ShowBase
base = ShowBase()
from direct.fsm.FSM import FSM
import sys

class SimpleFSM(FSM):
    def __init__(self):#optional because FSM already defines __init__
        # if you do write your own, you *must* call the base __init__ :
        FSM.__init__(self, 'SimpleFSM')

    def enterState01(self):
        print ("enter state 1")

    def exitState01(self):
        print ("exiting state 1")

    def enterStateExit(self):
        sys.exit(0)

fsm = SimpleFSM()

fsm.request("State01")
fsm.request("StateExit")

base.run()
