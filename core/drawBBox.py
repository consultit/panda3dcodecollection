"""This sample will show how to draw a box using the mouse
The box get's drawn when you click the 1st mouse button and drag the cursor in
any direction. After releasing the mouse button, the box will be removed again"""

from direct.showbase.ShowBase import ShowBase
from panda3d.core import LPoint2f, CardMaker, TransparencyAttrib

class BBox(ShowBase):
    def __init__(self):
        # usual game engine initialization
        ShowBase.__init__(self)
        self.disableMouse()

        # draw a model to show the alpha of the drawn box
        panda = loader.loadModel("panda")
        panda.setPos(0, 30, -5)
        panda.reparentTo(render)

        # accept the 1st mouse button events to start and stop the draw
        self.accept("mouse1", self.startBoxDraw)
        self.accept("mouse1-up", self.stopBoxDraw)
        # variables to store the start and current pos of the mousepointer
        self.startPos = LPoint2f(0,0)
        self.lastPos = LPoint2f(0,0)
        # variables for the to be drawn box
        self.boxCardMaker = CardMaker("SelectionBox")
        self.boxCardMaker.setColor(1,0,0,0.25)
        self.box = None

    def startBoxDraw(self):
        """Start drawing the box"""
        if self.mouseWatcherNode.hasMouse():
            # get the mouse position
            self.startPos = LPoint2f(self.mouseWatcherNode.getMouse())
        taskMgr.add(self.dragBoxDrawTask, "dragBoxDrawTask")

    def stopBoxDraw(self):
        """Stop the draw box task and remove the box"""
        taskMgr.remove("dragBoxDrawTask")
        self.box.removeNode()

    def dragBoxDrawTask(self, task):
        """This task will track the mouse position and actualize the box's size
        according to the first click position of the mouse"""
        if self.mouseWatcherNode.hasMouse():
            # get the current mouse position
            self.lastPos = LPoint2f(self.mouseWatcherNode.getMouse())

        # check if we already have a box
        if self.box != None:
            # if so, remove that old box
            self.box.removeNode()
        # set the box's size
        self.boxCardMaker.setFrame(
        	self.lastPos.getX(),
        	self.startPos.getX(),
        	self.startPos.getY(),
        	self.lastPos.getY())
        # generate, setup and draw the box
        node = self.boxCardMaker.generate()
        self.box = render2d.attachNewNode(node)
        self.box.setTransparency(TransparencyAttrib.M_alpha)

        # run until the task is manually stopped
        return task.cont

APP = BBox()
APP.run()
