#!/usr/bin/python
# -*- coding: utf-8 -*-

# Python imports
import os
import atexit
from direct.showbase.ShowBase import ShowBase
from panda3d.core import (
    Filename,
    ConfigPageManager,
    ConfigVariableBool,
    ConfigVariableDouble,
    ConfigVariableString,
    OFileStream,
    loadPrcFileData,
    loadPrcFile)

prcFile = "./MyConfig.prc"
# load the prc file if it already exist
if os.path.exists(prcFile):
    loadPrcFile(Filename.fromOsSpecific(prcFile))

class Main(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)

		# load config variables and set defaults if the variables do not exist
        myConfigVariableFloat = ConfigVariableDouble("double-variable",0.05).getValue()
        myConfigVariableBool = ConfigVariableBool("bool-variable", False).getValue()
        myConfigVariableString = ConfigVariableString("string-variable", "Foo").getValue()

        print ("CONFIGURATION:")
        print ("Float value -", myConfigVariableFloat)
        print ("Bool value -", myConfigVariableBool)
        print ("String value -", myConfigVariableString)

        # check if the config file hasn't been created
        if not os.path.exists(prcFile):
            self.__writeConfig()
        # register the write config function at application exit time
        atexit.register(self.__writeConfig)

    def __writeConfig(self):
        """Save current config in the prc file or if no prc file exists
        create one. The prc file is set in the prcFile variable"""
        page = None
        # Get the current values of the variables to be stored
        myConfigVariableFloat = str(0.05)
        myConfigVariableBool = False
        myConfigVariableString = "Foo"
        customConfigVariables = ["double-variable", "bool-variable", "string-variable"]
        if os.path.exists(prcFile):
            page = loadPrcFile(Filename.fromOsSpecific(prcFile))
            removeDecls = []
            for dec in range(page.getNumDeclarations()):
                # Check if our variables are given.
                # NOTE: This check has to be done to not loose our base or other
                #       manual config changes by the user
                if page.getVariableName(dec) in customConfigVariables :
                    decl = page.modifyDeclaration(dec)
                    removeDecls.append(decl)
            for dec in removeDecls:
                page.deleteDeclaration(dec)
            # floating point variable
            page.makeDeclaration("double-variable", myConfigVariableFloat)
            # boolean variable
            boolValue = "#f" if not myConfigVariableBool else "#t"
            page.makeDeclaration("bool-variable", boolValue)
            # string variable
            page.makeDeclaration("string-variable", myConfigVariableString)
        else:
            cpMgr = ConfigPageManager.getGlobalPtr()
            page = cpMgr.makeExplicitPage("my Pandaconfig")
            # floating point variable
            page.makeDeclaration("double-variable", myConfigVariableFloat)
            # boolean variable
            boolValue = "#f" if not myConfigVariableBool else "#t"
            page.makeDeclaration("bool-variable", boolValue)
            # string variable
            page.makeDeclaration("string-variable", myConfigVariableString)
        # create a stream to the specified config file
        configfile = OFileStream(prcFile)
        # and now write it out
        page.write(configfile)
        # close the stream
        configfile.close()

APP = Main()
APP.run()
