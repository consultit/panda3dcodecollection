"""simply throw and catch events"""

# import the engine stuff
from direct.showbase.ShowBase import ShowBase
base = ShowBase()
# DirectObject will be needed to accept events
from direct.showbase.DirectObject import DirectObject
# sys for exiting
import sys

class Catcher(DirectObject):
    """Catcher inherits DirectObject to be able to accept events"""
    def __init__(self):
        self.acceptEvents()

    def acceptEvents(self):
        # accept all the events we want to use
        self.accept("action", self.printAction)
        # this event will only be called once and then be ignored
        self.acceptOnce("oneAction", self.printActionOnce)
        # function call can also be a lambda. Not sys.exit as this will be called imideately
        self.accept("exit", lambda: sys.exit())

    def printAction(self):
        print("action")

    def printActionOnce(self):
        print("this will be called only once!")

    def ignoreEvents(self):
        # this will ignore all acceptd events in this class
        # to ignore specific events use self.ignore(eventname)
        self.ignoreAll()



#
# Testing
#
c = Catcher()
# send events that will be catched by everyone that accepts them.
messenger.send("action")
messenger.send("oneAction")
messenger.send("action")
# this event will not be catched hence no print output for it
messenger.send("oneAction")
c.ignoreEvents()
# this event will not be catched as the ignoreEvents function was called
messenger.send("action")
c.acceptEvents()
messenger.send("exit")
base.run()














