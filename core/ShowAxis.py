# This little script will show you how to add a global x/y/z axis on the
# bottom left side of the screen (like in most CAD applications). This
# axis will be displayed above any other objects

from direct.showbase.ShowBase import ShowBase
base = ShowBase()

# a node that is placed at the bottom left corner of the screen
corner = camera.attachNewNode("corner of screen")
corner.setPos(base.a2dLeft, 4, base.a2dBottom)

# load the axis that should be displayed
axis = loader.loadModel("zup-axis")
# make sure it will be drawn above all other elements
axis.setDepthTest(False)
axis.setBin("fixed",0)
axis.setScale(0.04)
# reparent it to the corner so it will always be placed there
axis.reparentTo(corner)
# now make sure it will stay in the correct rotation to render
axis.setCompass()

# load a test model
environ = loader.loadModel("environment")
environ.reparentTo(render)
environ.setScale(0.25, 0.25, 0.25)
environ.setPos(0, 0, 0)

base.run()
