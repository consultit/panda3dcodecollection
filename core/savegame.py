# This script shows a very simple way of storing game related data to be
# loaded again at a later time. For this it'll use pythons pickle
# functionality to create data dumps and writes them to a .sav file.
# For more complex save states you should may use database files instead
# of simple textfiles.

import os
import pickle

# this path tells the class where to store the save files
savePath = "."

class SaveGame():
    def __init__(self):
        self.donePercentage = 0

        self.levelDone = {
            "my first level": False,
            "my second level": False
        }

    def save(self, fileSlot):
        with open(os.path.join(savePath, fileSlot+".sav"), 'w') as sav:
            levelDone = pickle.dumps(self.levelDone)
            savestring = "{}~{}".format(
                levelDone,
                self.donePercentage)
            sav.write(savestring)

    def load(self, fileSlot):
        f = os.path.join(savePath, fileSlot+".sav")
        if not os.path.exists(f):
            return
        with open(f, 'r') as sav:
            saveData = sav.read().split("~")
            self.levelDone = pickle.loads(str.encode(saveData[0]))
            self.donePercentage = int(saveData[1])

#
# DEMO
#
def printSaveFile(savegame):
    print ("""savegame state:
    Levels Done {}
    {}% Done""".format(savegame.levelDone, savegame.donePercentage))

savegame1 = SaveGame()
printSaveFile(savegame1)
print ("")
print ("playing the game...")
savegame1.levelDone["my first level"] = True
savegame1.donePercentage = 50
print ("")
print ("savegame1 after playing:")
printSaveFile(savegame1)
print ("")
print ("saving savegame1...")
savegame1.save("demo_savefile1")
print ("game saved")
print ("")
print ("Initialize new save game state...")
print ("")
savegame2 = SaveGame()
print ("savegame2 Initial state:")
printSaveFile(savegame2)
print ("")
print ("loading demo_savefile1.sav into savegame2...")
savegame2.load("demo_savefile1")
print ("game loaded")
print ("")
print ("savegame2's state after loading")
printSaveFile(savegame2)
