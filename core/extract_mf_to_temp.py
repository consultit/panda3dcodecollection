""" This snippet will show how to simply extract all the data of a multifile
into the systems temp directory"""

# This is usefull if you have videos or other files, that can't directly be
# read from multifiles but you want to have stored in ones.

import os
from panda3d.core import VirtualFileSystem
from panda3d.core import Filename
from panda3d.core import Multifile
import tempfile


# temp path of the OS combined with a new test directory
tmpPath = Filename.fromOsSpecific(os.path.join(tempfile.gettempdir(), "test"))

# extract the multifile
baseDir = os.path.join("..", "data")
mf = Multifile()
mf.openRead(Filename(os.path.join(baseDir, "models.mf")))
for i in range(mf.getNumSubfiles()):
    path = os.path.join(tmpPath.getFullpath(), mf.getSubfileName(i))
    print ("extract to:", path)
    mf.extractSubfile(i, Filename(path))
print ("done")
mf.close()
