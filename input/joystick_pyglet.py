# This example shows how to enable joystick support using pyglet

# System
import sys
import os

# Panda3d
from direct.showbase.ShowBase import ShowBase

# pyglet
import pyglet



class Main(ShowBase):
    def __init__(self):
        # Engine Initialisations
        ShowBase.__init__(self, windowType='none')

        pyglet.app.platform_event_loop.start()
        self.joystick = None
        joysticks = pyglet.input.get_joysticks()
        if joysticks:
            self.joystick = joysticks[0]
        else:
            exit("No Joysticks/Gamepads Connected!")

        # set our functions that should be called on the respective events
        self.joystick.on_joybutton_press = self.on_joybutton_press
        self.joystick.on_joybutton_release = self.on_joybutton_release
        self.joystick.on_joyaxis_motion = self.on_joyaxis_motion
        self.joystick.on_joyhat_motion = self.on_joyhat_motion
        self.joystick.open()

        self.taskMgr.add(self.mainLoop, "main-loop-task")

    def on_joybutton_press(self, joystick, button):
        # this function will be called whenever a joystick button is pressed
        print "Button", joystick.button_controls[button].raw_name, "pressed on", joystick.device

    def on_joybutton_release(self, joystick, button):
        # this function will be called whenever a joystick button is released
        print "Button", joystick.button_controls[button].raw_name, "released on", joystick.device

    def on_joyaxis_motion(self, joystick, axis, value):
        # this function will be called whenever a joystick axis is moved
        print "Axis", axis, "on joystick", joystick.device, "changed to", value

    def on_joyhat_motion(self, joystick, hat_x, hat_y):
        # this function will be called whenever a joystick hat is moved
        print "Hat on joystick", joystick.device, "changed to X:", hat_x, "Y:", hat_y

    def mainLoop(self, task):
        # update pyglet loop
        pyglet.app.platform_event_loop.step(0.003)

        # Clear the console and print out all joystick stats
        """os.system("cls" if os.name=="nt" else "clear")
        print "x", self.joystick.x, self.joystick.x_control
        print "y", self.joystick.y, self.joystick.y_control
        print "z", self.joystick.z, self.joystick.z_control
        print "rx", self.joystick.rx, self.joystick.rx_control
        print "ry", self.joystick.ry, self.joystick.ry_control
        print "rz", self.joystick.rz, self.joystick.rz_control
        print "hat_x", self.joystick.hat_x, self.joystick.hat_x_control
        print "hat_y", self.joystick.hat_y, self.joystick.hat_y_control
        print "buttons", self.joystick.buttons
        print "button_controls:", self.joystick.button_controls
        """

        return task.cont

main = Main()
main.run()
