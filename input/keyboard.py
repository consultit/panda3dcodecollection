# display currently hit keyboard button big in the window

from direct.showbase.ShowBase import ShowBase
base = ShowBase()

# the most simple way
from direct.showbase.DirectObject import DirectObject
dobj = DirectObject()
dobj.accept("escape", lambda: exit())

# To catch generic key input use the button throwers like:
### Version 1
base.buttonThrowers[0].node().setButtonDownEvent("buttonDown")
def printKey(keyname):
    print (keyname)
dobj.accept("buttonDown", printKey)
### Version 2
base.buttonThrowers[0].node().setKeystrokeEvent("keystroke")
def printKeystroke(keyname):
    print ("keystroke", keyname)
dobj.accept("keystroke", printKeystroke)

base.run()
