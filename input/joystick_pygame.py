# This example shows how to enable joystick support using pygame

# System
import sys

# Panda3d
from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor

# Pygame
import pygame



class Main(ShowBase):
    def __init__(self):
        # Engine Initialisations
        ShowBase.__init__(self, windowType='none')
        pygame.init()

        # accept quit
        self.accept("escape", sys.exit)

        # initialize controls
        joysticks = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
        #print "joysticks:", joysticks
        if len(joysticks) <= 0: sys.exit("No Joysticks found!")
        self.mainJoystick = joysticks[0]
        self.mainJoystick.init()
        #print self.mainJoystick.get_init()
        #print self.mainJoystick.get_id()
        #print self.mainJoystick.get_name()
        #print self.mainJoystick.get_numaxes()
        #print self.mainJoystick.get_numballs()
        #print self.mainJoystick.get_numbuttons()
        #print self.mainJoystick.get_numhats()
        axisPoint = 0.0
        axisPoint = self.mainJoystick.get_axis(0)
        print "joystick-axis 0:", axisPoint

        self.taskMgr.add(self.mainLoop, "main-loop-task")

    def mainLoop(self, task):
        for event in pygame.event.get():
            # Possible joystick actions: JOYAXISMOTION JOYBALLMOTION JOYBUTTONDOWN JOYBUTTONUP JOYHATMOTION
            if event.type == pygame.JOYBUTTONDOWN:
                print("Joystick button pressed.")
                for button in range(self.mainJoystick.get_numbuttons()):
                    if self.mainJoystick.get_button(button):
                        print button, "=", self.mainJoystick.get_button(button)
            if event.type == pygame.JOYBUTTONUP:
                print("Joystick button released.")
                for button in range(self.mainJoystick.get_numbuttons()):
                    if self.mainJoystick.get_button(button):
                        print button, "=", self.mainJoystick.get_button(button)
            if event.type == pygame.JOYAXISMOTION:
                for axis in range(self.mainJoystick.get_numaxes()):
                    axisChange = 0.0
                    axisChange = self.mainJoystick.get_axis(axis)
                    if axisChange != 0.0:
                        print axis, "=", axisChange


        ## for button in range(self.mainJoystick.get_numbuttons()):
        ##     if self.mainJoystick.get_button(button):
        ##         print button, "=", self.mainJoystick.get_button(button)

        ## for axis in range(self.mainJoystick.get_numaxes()):
        ##     axisChange = 0.0
        ##     axisChange = self.mainJoystick.get_axis(axis)
        ##     if axisChange != 0.0:
        ##         print axis, "=", axisChange




        return task.cont

main = Main()
main.run()
