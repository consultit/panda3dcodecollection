#display mouseposition and clicked mouse buttons

from direct.showbase.ShowBase import ShowBase
base = ShowBase()
#from panda3d.core import NodePath

def click(key):
    print (key, "clicked")

base.accept("mouse1", click, ["mouse1"])
base.accept("mouse2", click, ["mouse2"])
base.accept("mouse3", click, ["mouse3"])
base.accept("wheel_up", click, ["wheel_up"])
base.accept("wheel_down", click, ["wheel_down"])

def showInfo():
    if base.mouseWatcherNode.hasMouse():
        x = base.mouseWatcherNode.getMouseX()
        y = base.mouseWatcherNode.getMouseY()
    print ("mouse position x y", x, y)

base.accept("f1", showInfo)

base.run()
