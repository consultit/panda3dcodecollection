from direct.showbase.ShowBase import ShowBase
from direct.showbase import Audio3DManager
from panda3d.core import CollisionTraverser

class Audiotest(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        # accept the esc button to close the application
        self.accept("escape", exit)

        # create a 3D audio manager that uses the default sfx manager
        # and sets the camera as listener
        self.sfx3dManager = Audio3DManager.Audio3DManager(base.sfxManagerList[0], camera)

        # create a node that the sound will rotate around
        center = render.attachNewNode("Center")
        rotationIval = center.hprInterval(20, (360, 0, 0))

        # load the smiley model as visual representation of
        # the audio position and place it a bit off of the center.
        smiley = loader.loadModel("smiley")
        smiley.setY(15)
        smiley.reparentTo(center)

        # start the rotation
        rotationIval.loop()

        # now load the audio, attach it to the smiley and start playback
        audio = self.sfx3dManager.loadSfx("../../data/audio/sample_mono.ogg")
        self.sfx3dManager.attachSoundToObject(audio, smiley)
        self.sfx3dManager.setDistanceFactor(1)

        # create a collision traverser for the automatic velocity setup
        base.cTrav = CollisionTraverser()
        # set the velocity to be handled automatically for our audio file
        self.sfx3dManager.setSoundVelocityAuto(audio)
        self.sfx3dManager.setListenerVelocityAuto()

        # Test for panning
        audio.setLoop(True)
        audio.play()

APP = Audiotest()
APP.run()
