from espeak import espeak
import time

espeak.set_parameter(espeak.Parameter.Rate, 120)
espeak.synth("""The quick brown fox jumps over the lazy dog.""")
while espeak.is_playing():
    time.sleep
