#NOTE: uses the ControlManager and the *Walker modules which are shipped
#with p3d by default and look like were used by the toontown online game

#TODO: check how to setup the walkers
#TODO 2: commented out code (line 471 in walker classes)

#NOTE: This is a work in progres file and currently doesn't work

import sys

from direct.showbase.ShowBase import ShowBase
from direct.showbase.DirectObject import DirectObject
from panda3d.core import BitMask32
from panda3d.core import CollisionPlane, CollisionNode, CollisionTraverser
from panda3d.core import Vec3, Point3, Plane

class Main(ShowBase, DirectObject):
    def __init__(self):
        ShowBase.__init__(self)
        # accept the esc button to close the application
        self.accept("escape", sys.exit)

        # Basic Collision detection setup
        traverser = CollisionTraverser('main traverser')
        base.cTrav = traverser

        wallBitmask = BitMask32.bit(1)
        floorBitmask = BitMask32.bit(1)

        # A ground for our Player to walk on
        plane = CollisionPlane(Plane(Vec3(0, 0, 1), Point3(0, 0, -2)))
        floor = render.attachNewNode(CollisionNode('floor'))
        floor.node().addSolid(plane)
        floor.node().setCollideMask(floorBitmask)
        floor.show()

        # render the collisions to make the vissible
        base.cTrav.showCollisions(render)

        # import the control manager and walker classes that we want to use
        from direct.controls.ControlManager import ControlManager
        from direct.controls.PhysicsWalker import PhysicsWalker

        actor = loader.loadModel("smiley")
        actor.setPos(0, 15, 0)
        actor.reparentTo(render)

        pw = PhysicsWalker()
        pw.initializeCollisions(base.cTrav, actor, wallBitmask,
            floorBitmask, avatarRadius = 1.0, floorOffset = 0.25,
            reach = 0.25)
        pwName = "Physics Walker Controls"

        cm = ControlManager()
        cm.add(pw, pwName)

        cm.use(pwName, actor)

APP = Main()
APP.run()
