"""
This character sample class is usefull for 3rd person platformer type games,
it has the following camera snippet attached and the controls are set up to
possibly work fine with gamepad controller input
"""

import math
from direct.showbase.ShowBase import ShowBase
from direct.actor.Actor import Actor
from direct.fsm.FSM import FSM
from panda3d.core import (
    NodePath,
    PandaNode,
    Vec3)

class Player(ShowBase, FSM):
    def __init__(self):
        ShowBase.__init__(self)
        FSM.__init__(self, "FSM-Player")

        self.disableMouse()

        #
        # PLAYER CONTROLS AND CAMERA
        #
        self.player = Actor("../data/models/wolf", {
            "Idle":"../data/models/wolf-idle",
            "Walk":"../data/models/wolf-walk"})
        self.player.setScale(0.05)
        self.player.setBlend(frameBlend = True)
        # the next two vars will set the min and max distance the cam can have
        # to the node it is attached to
        self.maxCamDistance = 4.0
        self.minCamDistance = 3.0
        # the initial cam distance
        self.camDistance = (self.maxCamDistance - self.minCamDistance) / 2.0 + self.minCamDistance
        # an invisible object which will fly above the player and will be used to
        # track the camera on it
        self.camFloater = NodePath(PandaNode("playerCamFloater"))
        self.camFloater.setPos(0,0,1.5)
        self.camFloater.reparentTo(self.player)

        self.environment = loader.loadModel("environment")
        self.environment.setPos(0, 0, -1)

    def start(self):
        self.player.reparentTo(render)
        self.environment.reparentTo(render)

        self.keyMap = {"horizontal":0, "vertical":0}

        self.accept("escape", exit)
        self.accept("a", self.setKey, ["horizontal",1])
        self.accept("d", self.setKey, ["horizontal",-1])
        self.accept("w", self.setKey, ["vertical",-1])
        self.accept("s", self.setKey, ["vertical",1])
        self.accept("a-up", self.setKey, ["horizontal",0])
        self.accept("d-up", self.setKey, ["horizontal",0])
        self.accept("w-up", self.setKey, ["vertical",0])
        self.accept("s-up", self.setKey, ["vertical",0])

        taskMgr.add(self.move, "task_movement", priority=-10)
        taskMgr.add(self.updateCam, "task_camActualisation", priority=-4)

        camera.setPos(self.player, 0, self.camDistance, 20)

        self.request("Idle")

    #
    # MOVE FUNCTIONS
    #
    def setKey(self, key, value):
        self.keyMap[key] = value

    def move(self, task):
        dt = globalClock.getDt()
        requestState = "Idle"


        move = False
        if self.keyMap["horizontal"] != 0:
            requestState = "Walk"
            move = True
        if self.keyMap["vertical"] != 0:
            requestState = "Walk"
            move = True
        if move:
            movementVec = Vec3(
                self.keyMap["horizontal"],
                self.keyMap["vertical"],
                0)
            angle = math.atan2(-movementVec.getX(), movementVec.getY())
            rotation = angle * (180.0/math.pi)
            self.player.setH(self.camera, rotation)
            self.player.setP(0)
            self.player.setR(0)
            self.player.setPos(self.player,(0, -30*dt, 0))

        if self.state != requestState:
            self.request(requestState)
        return task.cont

    #
    # CAMERA FUNCTIONS
    #
    def updateCam(self, task):
        """This function will check the min and max distance of the camera to
        the defined model and will correct the position if the cam is to close
        or to far away"""

        # Camera Movement Updates
        camvec = self.player.getPos() - camera.getPos()
        camvec.setZ(0)
        camdist = camvec.length()
        camvec.normalize()

        # If far from player start following
        if camdist > self.maxCamDistance:
            camera.setPos(camera.getPos() + camvec*(camdist-self.maxCamDistance))
            camdist = self.maxCamDistance

        # If player to close move cam backwards
        if camdist < self.minCamDistance:
            camera.setPos(camera.getPos() - camvec*(self.minCamDistance-camdist))
            camdist = self.minCamDistance

        camera.lookAt(self.camFloater)
        return task.cont

    #
    # FSM FUNCTIONS
    #
    def enterIdle(self):
        self.player.loop("Idle")

    def enterWalk(self):
        self.player.loop("Walk")

APP = Player()
APP.start()
APP.run()
