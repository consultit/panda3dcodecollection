"""take screenshots of the content of a panda3d window"""

from direct.showbase.ShowBase import ShowBase
base = ShowBase()
from panda3d.core import Filename
import time

def screenshot():
    """This function will save a screenshot with the current date and time
    in the current directory"""
    # get the time for the screenshot
    date = time.time()
    # setup the full name (incl. path and fileending) for the screenshot
    name = Filename("./Screenshot#{0}.jpg".format(date))
    # now the actual saving is done
    base.win.saveScreenshot(name)
    print ("screenshot taken")

# to test the screenshot function, simply load a model
panda = loader.loadModel("panda")
panda.reparentTo(render)
base.disableMouse()
camera.setPos(0,-25,6)
base.graphicsEngine.renderFrame()
base.graphicsEngine.renderFrame()

# take a screenshot of the scene
screenshot()

base.run()
