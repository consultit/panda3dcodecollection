#!/usr/bin/python
# -*- coding: utf-8 -*-

from direct.showbase.ShowBase import ShowBase
from direct.showbase.DirectObject import DirectObject

class WindowHandling(ShowBase):
    def __init__(self):
        # initialize the engine
        ShowBase.__init__(self)

        do = DirectObject()
        do.accept("PandaPaused", self.minimized)
        #self.accept(base.win.getWindowEvent(), self.windowEvent)

        do.accept("window-event", self.windowEvent)
        #taskMgr.add(self.windowTask, "windoweventtask")

    def minimized(self):
        print("PAUSED")

    def windowEvent(self, window):
        print("event cought from {}".format(window))
        print("minimized A = {}".format(base.mainWinMinimized))
        properties = window.getProperties()

        print("minimized B = {}".format(properties.getMinimized()))#base.mainWinMinimized))

    def windowTask(self, task):
        properties = base.win.getProperties()
        if properties.getMinimized():
            print("minimized = {}".format(properties.getMinimized()))
            return task.done
        else:
            print("not minimized")
        return task.cont

windowhandling = WindowHandling()
windowhandling.run()
