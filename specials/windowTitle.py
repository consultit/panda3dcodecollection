"""This sample will show how to change the window title and icon"""

#from panda3d.core import loadPrcFileData
#loadPrcFileData("", "icon-filename ../data/icon/app/app.ico")

from direct.showbase.ShowBase import ShowBase
base = ShowBase()
from panda3d.core import Filename
from panda3d.core import WindowProperties

props = WindowProperties()
props.setIconFilename(Filename("../data/icon/app/app.ico"))
props.setTitle("My APP")
base.win.requestProperties(props)


base.run()
