"""Toggles the window between fullscreen and windowed mode"""

from direct.showbase.ShowBase import ShowBase
base = ShowBase()
from panda3d.core import WindowProperties

props = WindowProperties()

# save the initial window size
windowSizeX = base.win.getXSize()
windowSizeY = base.win.getYSize()

def toggleFullscreen():
    # global variables... in production code, better use class variables!
    global windowSizeX
    global windowSizeY

    # get the window properties and clear them
    props = WindowProperties()
    props.clear()
    props.clearFullscreen()
    props.clearSize()

    # are we fullscreen yet
    fullscreen = base.win.isFullscreen()
    # for clarity set a variable that determines to which state
    # we want to switch
    changeToFullscreen = not fullscreen

    # if we are in windowed mode, save the current window size for
    # resetting it later
    if not fullscreen:
        windowSizeX = base.win.getXSize()
        windowSizeY = base.win.getYSize()

    # set the new window size
    x = windowSizeX
    y = windowSizeY
    if changeToFullscreen:
        di = base.pipe.getDisplayInformation()
        x = di.getDisplayModeWidth(0)
        y = di.getDisplayModeHeight(0)
    props.setSize(x, y)

    # set the fullscreen property
    props.setFullscreen(changeToFullscreen)
    props.setUndecorated(changeToFullscreen)

    base.win.requestProperties(props)
    base.taskMgr.step()


# setup f11 to toggle fullscreen
from direct.showbase.DirectObject import DirectObject
dobj = DirectObject()
dobj.accept("f11", toggleFullscreen)


base.run()
