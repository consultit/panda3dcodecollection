""" set graphic quality like antialias resolution etc... """

#
# ANTIALIAS
#
# important to set these if multisample antialias should be used
from panda3d.core import loadPrcFileData
loadPrcFileData("", "framebuffer-multisample 1")
loadPrcFileData("", "multisamples 4")
loadPrcFileData("", "texture-anisotropic-degree 4")

from direct.showbase.ShowBase import ShowBase
base = ShowBase()
from panda3d.core import AntialiasAttrib
from panda3d.core import DirectionalLight
from panda3d.core import WindowProperties

#
# WINDOW RESOLUTION
#
def setResolution(resx, resy):
    # set the resolution of the window or monitor if in fullscreen
    props = WindowProperties()
    # set resolution
    props.setSize(resx, resy)
    base.win.requestProperties(props)


#
# LIGHTING
#
# set shadow map quality
shadowMapSize = 512
dlight = DirectionalLight('dlight')
dlight.setShadowCaster(True, shadowMapSize, shadowMapSize)


#
# ANTIALIAS
#
def setAntialias(active):
    # set and unset antialias
    if active:
        base.render.setAntialias(AntialiasAttrib.MAuto)
    else:
        base.render.clearAntialias()


#
# SHADERS
#
def setShader(active):
    if not active:
        # remove shaders
        base.render.clearShader()
        return
    # activate panda3ds auto shader system
    if (base.win.getGsg().getSupportsBasicShaders() != 0):
        # shaders are supported
        base.render.setShaderAuto()
    else:
        # shaders are not supported
        print ("your graphics card does not support shaders")


# setup some keys to change quality
from direct.showbase.DirectObject import DirectObject
dobj = DirectObject()
# resolution changes
dobj.accept("f1", setResolution, extraArgs=[600, 480])
dobj.accept("f2", setResolution, extraArgs=[800, 600])
# antialias on/off
dobj.accept("f3", setAntialias, extraArgs=[True])
dobj.accept("f4", setAntialias, extraArgs=[False])
# shaders on/off
dobj.accept("f5", setShader, extraArgs=[True])
dobj.accept("f6", setShader, extraArgs=[False])


panda = loader.loadModel("panda")
panda.setPos(0, 30, -7)
panda.reparentTo(render)

base.run()
